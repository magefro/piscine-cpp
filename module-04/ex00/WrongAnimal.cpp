//
// Created by Antoine LANGLOIS on 2/9/22.
//

#include "WrongAnimal.hpp"


//-------------------- CONSTRUCTORS ----------------------

WrongAnimal::WrongAnimal() : type("WrongDefault") {

	std::cout << "WrongAnimal constructor called" << std::endl;
	return ;
}

WrongAnimal::~WrongAnimal() {

	std::cout << "WrongAnimal destructor called" << std::endl;
	return ;
}

WrongAnimal::WrongAnimal( WrongAnimal const & w ) {

	std::cout << "WrongAnimal copy constructor called" << std::endl;
	*this = w;
	return ;
}

//-------------------- OPERATORS --------------------------

WrongAnimal &WrongAnimal::operator=( WrongAnimal const & w ) {

	std::cout << "WrongAnimal assignation operator called" << std::endl;
	if (this != &w)
		this->type = w.getType();
	return (*this);
}

void WrongAnimal::makeSound( void ) const {

	std::cout << "WrongAnimal sound" << std::endl;
	return ;
}

//------------------- GETTERS / SETTERS -------------------

std::string WrongAnimal::getType( void ) const {

	return (this->type);
}

void 		WrongAnimal::setType( std::string const type ) {

	this->type = type;
}