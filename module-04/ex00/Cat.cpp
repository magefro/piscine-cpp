//
// Created by antoine on 07/02/2022.
//

#include "Cat.hpp"
//------------------- CONSTRUCTORS ----------------

Cat::Cat( void ) : Animal() {

	std::cout << "Cat default constructor called" << std::endl;
	this->setType("Cat");
	return ;
}

Cat::~Cat( void ) {

	std::cout << "Cat destructor called" << std::endl;
	return ;
}

Cat::Cat( Cat const & c ) : Animal() {

	std::cout << "Cat copy constructor called" << std::endl;
	*this = c;
	return ;
}

//---------------- OPERATORS ---------------------

Cat &Cat::operator=( Cat const & c ) {

	std::cout << "Cat assignation operator called" << std::endl;
	if (this != &c)
		this->type = c.getType();
	return (*this);
}

//--------------- FUNCTIONS ---------------------

void Cat::makeSound( void ) const {

	std::cout << "Miaou miaou mimimi" << std::endl;
	return ;
}