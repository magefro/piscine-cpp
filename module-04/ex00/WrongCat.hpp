//
// Created by Antoine LANGLOIS on 2/9/22.
//

#ifndef PISCINE_CPP_WRONGCAT_HPP
#define PISCINE_CPP_WRONGCAT_HPP

#include <iostream>
#include "WrongAnimal.hpp"

class WrongCat : public WrongAnimal {

public:
	WrongCat();
	~WrongCat();
	WrongCat( WrongCat const & w );

	WrongCat &operator=( WrongCat const & w );

	void makeSound( void ) const ;
};


#endif //PISCINE_CPP_WRONGCAT_HPP
