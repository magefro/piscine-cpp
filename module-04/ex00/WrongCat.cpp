//
// Created by Antoine LANGLOIS on 2/9/22.
//

#include "WrongCat.hpp"

//-------------------------- CONSTRUCTORS -------------------

WrongCat::WrongCat() : WrongAnimal() {

	std::cout << "WrongCat constructor called" << std::endl;
	this->setType("WrongCat");
	return ;
}

WrongCat::~WrongCat() {

	std::cout << "WrongCat destructor called" << std::endl;
	return ;
}

WrongCat::WrongCat( WrongCat const & w ) : WrongAnimal () {

	std::cout << "WrongCat copy constructor called" << std::endl;
	*this = w;
	return ;
}

//------------------------ OPERATORS ---------------------------

WrongCat &WrongCat::operator=( WrongCat const & w ) {

	std::cout << "WrongCat assignation operator called" << std::endl;
	if (this != &w)
		this->type = w.getType();
	return (*this);
}

//----------------------- FUNCTIONS --------------------------

void WrongCat::makeSound( void ) const {

	std::cout << "WrongCat sound" << std::endl;
	return ;
}