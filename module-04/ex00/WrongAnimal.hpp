//
// Created by Antoine LANGLOIS on 2/9/22.
//

#ifndef PISCINE_CPP_WRONGANIMAL_HPP
#define PISCINE_CPP_WRONGANIMAL_HPP

#include <iostream>

class WrongAnimal {

protected:
	std::string type;

public:
	WrongAnimal();
	~WrongAnimal();
	WrongAnimal( WrongAnimal const & w );

	WrongAnimal &operator=( WrongAnimal const & w );

	void makeSound( void ) const ;

	std::string getType( void ) const;
	void 		setType( std::string const type );
};


#endif //PISCINE_CPP_WRONGANIMAL_HPP
