//
// Created by antoine on 07/02/2022.
//

#include "Dog.hpp"

//----------------- CONSTRUCTORS --------------------

Dog::Dog( void ) : Animal() {

	std::cout << "Dog default constructor called" << std::endl;
	this->setType("Dog");
	return ;
}

Dog::~Dog( void ) {

	std::cout << "Dog destructor called" << std::endl;
	return ;
}

Dog::Dog( Dog const & d ) : Animal() {

	std::cout << "Dog copy constructor called" << std::endl;
	*this = d;
	return ;
}

//---------------- OPERATORS ---------------------

Dog &Dog::operator=( Dog const & d ) {

	std::cout << "Dog assignation operator called" << std::endl;
	if (this != &d)
		this->type = d.getType();
	return (*this);
}

//--------------- FUNCTIONS ----------------------

void Dog::makeSound( void ) const {

	std::cout << "Ouaf ouaf grrrrrr" << std::endl;
	return ;
}