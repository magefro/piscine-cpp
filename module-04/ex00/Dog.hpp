//
// Created by antoine on 07/02/2022.
//

#ifndef PISCINE_CPP_DOG_HPP
#define PISCINE_CPP_DOG_HPP

#include "Animal.hpp"
#include <iostream>

class Dog : public Animal {

public:
	Dog( void );
	~Dog( void );
	Dog( Dog const & d );

	Dog &operator=( Dog const & c );

	void makeSound( void ) const;

};


#endif //PISCINE_CPP_DOG_HPP
