//
// Created by Antoine LANGLOIS on 2/9/22.
//

#include "Animal.hpp"
#include "Dog.hpp"
#include "Cat.hpp"
#include "WrongAnimal.hpp"
#include "WrongCat.hpp"

int main() {

	const Animal *plouf = new Animal();
	const Animal *miaouss = new Cat();
	const Animal *rex = new Dog();
	const WrongAnimal *bibi = new WrongAnimal();
	const WrongAnimal *pouet = new WrongCat();

	std::cout << std::endl;
	std::cout << "--------" << std::endl;
	std::cout << plouf->getType() << std::endl;
	plouf->makeSound();
	std::cout << miaouss->getType() << std::endl;
	miaouss->makeSound();
	std::cout << rex->getType() << std::endl;
	rex->makeSound();
	std::cout << "--------" << std::endl;
	std::cout << bibi->getType() << std::endl;
	bibi->makeSound();
	std::cout << pouet->getType() << std::endl;
	pouet->makeSound();
	std::cout << std::endl;

	delete plouf;
	delete miaouss;
	delete rex;
	delete pouet;
	delete bibi;

	return (0);
}