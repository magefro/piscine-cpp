//
// Created by antoine on 07/02/2022.
//

#include "Animal.hpp"

//------------------ CONSTRUCTORS -------------------------

Animal::Animal( void ) : type("Default") {

	std::cout << "Animal default constructor called" << std::endl;
	return ;
}

Animal::~Animal( void ) {

	std::cout << "Animal destructor called" << std::endl;
	return ;
}
Animal::Animal( Animal const & a ) {

	std::cout << "Animal copy constructor called" << std::endl;
	*this = a;
	return ;
}

//------------------ OPERATORS -------------------------

Animal &Animal::operator=( Animal const & a ) {

	std::cout << "Animal assignation operator called" << std::endl;
	if (this != &a)
		this->type = a.getType();
	return (*this);
}

//------------------------ ACTIONS ----------------------------

void Animal::makeSound( void ) const {

	std::cout << "Thats the sound of the animal whoop whoop" << std::endl;
	return ;
}

//------------------ GETTERS / SETTERS ---------------------

std::string Animal::getType( void ) const {

	return (this->type);
}

void Animal::setType( std::string const type ) {

	this->type = type;
	return ;
}