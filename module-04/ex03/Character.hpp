//
// Created by Antoine LANGLOIS on 2/15/22.
//

#ifndef PISCINE_CPP_CHARACTER_HPP
#define PISCINE_CPP_CHARACTER_HPP

#include "ICharacter.hpp"
#include "AMateria.hpp"



class Character : public ICharacter {

protected:
	std::string name;
	AMateria *slots[4];

public:
	Character( std::string name );
	Character( Character const & c );
	Character();
	~Character();

	Character &operator=( Character const & c );

	std::string const & getName() const;
	void equip( AMateria * m );
	void unequip( int idx );
	void use( int idx, ICharacter & target );
};


#endif //PISCINE_CPP_CHARACTER_HPP
