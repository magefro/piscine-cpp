//
// Created by Antoine LANGLOIS on 2/15/22.
//

#ifndef PISCINE_CPP_CURE_HPP
#define PISCINE_CPP_CURE_HPP

#include "AMateria.hpp"

class Cure : public AMateria {

public:
	Cure( Cure const & c );
	Cure();
	~Cure();

	Cure &operator=( Cure const & c );

	std::string getType() const;
	void setType( std::string type );

	void use( ICharacter & target );
	AMateria *clone() const;
};


#endif //PISCINE_CPP_CURE_HPP
