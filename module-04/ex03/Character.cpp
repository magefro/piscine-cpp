//
// Created by Antoine LANGLOIS on 2/15/22.
//

#include "ICharacter.hpp"
#include "Character.hpp"

//------------------ CONSTRUCTORS ------------------

Character::Character( std::string name ) : name(name) {

	std::cout << "Character constructor called" << std::endl;
	for (int i = 0; i < 4; i++)
		this->slots[i] = NULL;
	return ;
}

Character::Character( Character const & c ) {

	std::cout << "Character copy constructor called" << std::endl;
	this->name = c.getName();
	for (int i = 0; i < 4; i++)
		this->slots[i] = c.slots[i];
	return ;
}

Character::Character() : name("Default") {

	std::cout << "Character default constructor called" << std::endl;
	for (int i = 0; i < 4; i++)
		slots[i] = NULL;
	return ;
}

Character::~Character() {

	std::cout << "Character destructor called" << std::endl;
	for (int i = 0; i < 4; i++)
		this->slots[i] = NULL;
	return ;
}

//------------------ OPERATORS ---------------------

Character &Character::operator=( Character const & c ) {

	std::cout << "Character assignation operator called" << std::endl;
	if (this != &c)
	{
		this->name = c.getName();
		for (int i = 0; i < 4; i++)
			this->slots[i] = c.slots[i];
	}
	return (*this);
}

//----------------- GETTERS ------------------------

std::string const & Character::getName() const {

	return (this->name);
}

//------------------ ACTIONS ----------------------

void Character::equip( AMateria * m ) {

	if (m == NULL)
	{
		std::cout << "Cannot equip empty materia" << std::endl;
		return ;
	}
	for (int i = 0; i < 4; i++)
	{
		if (this->slots[i] == NULL)
		{
			std::cout << "Equiping Materia to character slot " << i << std::endl;
			this->slots[i] = m;
			return ;
		}
	}
	std::cout << "Error : Materia slots are full" << std::endl;
	return ;
}

void Character::unequip( int idx ) {

	if (idx >= 0 && idx < 4 && this->slots[idx] != NULL)
	{
		std::cout << "Unequiping Materia" << std::endl;
		this->slots[idx] = NULL;
	}
	else
		std::cout << "Error: Cannot unequip Materia" << std::endl;
	return ;
}

void Character::use( int idx, ICharacter & target ) {

	if (idx >= 0 && idx < 4 && this->slots[idx] != NULL)
		this->slots[idx]->use(target);
	else
		std::cout << "Error: Cannot use Materia" << std::endl;
	return ;
}