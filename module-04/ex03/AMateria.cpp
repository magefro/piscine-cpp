//
// Created by Antoine LANGLOIS on 2/15/22.
//

#include "AMateria.hpp"

//----------------- CONSTRUCTORS ------------------

AMateria::AMateria( std::string const & type ) : type(type) {

	std::cout << "AMateria constructor called" << std::endl;
	return ;
}

AMateria::AMateria( AMateria const & a ) {

	std::cout << "AMateria copy constructor called" << std::endl;
	this->type = a.getType();
	return ;
}

AMateria::AMateria() : type("undefined") {

	std::cout << "AMateria default constructor called" << std::endl;
	return ;
}

AMateria::~AMateria() {

	std::cout << "AMateria destructor called" << std::endl;
	return ;
}

//----------------- OPERATORS ---------------------

AMateria &AMateria::operator=( AMateria const & a ) {

	std::cout << "AMateria assignation operator called" << std::endl;
	if (this != &a)
	{
		this->type = a.getType();
	}
	return (*this);
}

//---------------- GETTERS ------------------------

std::string const & AMateria::getType() const {

	return (this->type);
}

//--------------- ABSTRACT -----------------------

void AMateria::use( ICharacter & target ) {

	std::cout << target.getName() << " got an undefined materia" << std::endl;
	return ;
}