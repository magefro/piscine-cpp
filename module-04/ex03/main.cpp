//
// Created by Antoine LANGLOIS on 2/15/22.
//

#include "IMateriaSource.hpp"
#include "MateriaSource.hpp"
#include "AMateria.hpp"
#include "ICharacter.hpp"
#include "Character.hpp"
#include "Ice.hpp"
#include "Cure.hpp"

int main() {

	std::cout << "Creating new character bob:" << std::endl;
	ICharacter *bob = new Character("bob");

	std::cout << std::endl << "Creating new character kiki:" << std::endl;
	ICharacter *kiki = new Character("kiki");

	std::cout << std::endl << "bob name: " << bob->getName() << std::endl;
	std::cout << "kiki name: " << kiki->getName() << std::endl;

	std::cout << std::endl << "bob use free materia slot:" << std::endl;
	bob->use(0, *kiki);

	std::cout << std::endl << "bob equips empty materia:" << std::endl;
	AMateria *materia_0 = NULL;
	bob->equip(materia_0);

	std::cout << std::endl << "Creating new materia ice:" << std::endl;
	AMateria *materia_1 = new Ice();

	std::cout << std::endl << "bob equips materia ice:" << std::endl;
	bob->equip(materia_1);

	std::cout << std::endl << "bob use materia ice:" << std::endl;
	bob->use(0, *kiki);

	std::cout << std::endl << "Creating new materia cure:" << std::endl;
	AMateria *materia_2 = new Cure();

	std::cout << std::endl << "bob equips materia cure:" << std::endl;
	bob->equip(materia_2);

	std::cout << std::endl << "bob use materia ice, then cure, then fail:" << std::endl;
	bob->use(0, *kiki);
	bob->use(1, *kiki);
	bob->use(2, *kiki);

	std::cout << std::endl << "cloning materia ice to new materia, then equip:" << std::endl;
	AMateria *materia_3 = materia_1->clone();
	bob->equip(materia_3);

	std::cout << std::endl << "again until full:" << std::endl;
	AMateria *materia_4 = materia_1->clone();
	bob->equip(materia_4);
	AMateria *materia_5 = materia_1->clone();
	bob->equip(materia_5);

	std::cout << std::endl << "bob use all materia until max:" << std::endl;
	bob->use(3, *kiki);
	bob->use(4, *kiki);
	bob->use(5, *kiki);

	std::cout << std::endl << "Creating new materia source:" << std::endl;
	IMateriaSource *source = new MateriaSource();

	std::cout << std::endl << "Learning new materia source:" << std::endl;
	source->learnMateria(materia_1);
	source->learnMateria(materia_2);
	source->learnMateria(materia_3);
	source->learnMateria(materia_4);
	source->learnMateria(materia_5);

	std::cout << std::endl << "Creating materia with unknown name:" << std::endl;
	AMateria *newMat_1 = source->createMateria("prout");

	std::cout << std::endl << "Creating materia ice:" << std::endl;
	newMat_1 = source->createMateria("ice");

	std::cout << std::endl << "Creating materia cure:" << std::endl;
	AMateria *newMat_2 = source->createMateria("cure");

	std::cout << std::endl << "equiping new materias to kiki:" << std::endl;
	kiki->equip(newMat_1);
	kiki->equip(newMat_2);

	std::cout << std::endl << "kiki uses materias:" << std::endl;
	kiki->use(0, *bob);
	kiki->use(1, *bob);
	kiki->use(2, *bob);

	std::cout << std::endl << "new character assigning kiki materias:" << std::endl;
	ICharacter *toto;
	toto = kiki;
	toto->use(0, *bob);
	toto->use(1, *bob);
	toto->use(2, *bob);

	std::cout << std::endl << "kiki unequip materias:" << std::endl;
	kiki->unequip(0);
	kiki->unequip(1);
	kiki->unequip(2);

	std::cout << std::endl << "kiki uses again materias:" << std::endl;
	kiki->use(0, *bob);
	kiki->use(1, *bob);

	std::cout << std::endl << "deleting materias:" << std::endl;
	delete materia_1;
	delete materia_2;
	delete materia_3;
	delete materia_4;
	delete materia_5;

	std::cout << std::endl << "deleting materia source:" << std::endl;
	delete source;

	std::cout << std::endl << "deleting characters:" << std::endl;
	delete bob;
	delete kiki;

	return (0);
}