//
// Created by Antoine LANGLOIS on 2/15/22.
//

#ifndef PISCINE_CPP_ICE_HPP
#define PISCINE_CPP_ICE_HPP

#include "AMateria.hpp"

class Ice : public AMateria {

public:
	Ice( Ice const & i );
	Ice();
	~Ice();

	Ice &operator=( Ice const & i );

	std::string getType() const;
	void setType( std::string type );

	void use( ICharacter & target );
	AMateria *clone() const;
};


#endif //PISCINE_CPP_ICE_HPP
