//
// Created by Antoine LANGLOIS on 2/15/22.
//

#ifndef PISCINE_CPP_MATERIASOURCE_HPP
#define PISCINE_CPP_MATERIASOURCE_HPP

#include "IMateriaSource.hpp"
#include "AMateria.hpp"

class MateriaSource : public IMateriaSource {

protected:
	AMateria *materias[4];

public:
	MateriaSource();
	~MateriaSource();
	MateriaSource( MateriaSource const & m );

	MateriaSource &operator=( MateriaSource const & m );

	void learnMateria( AMateria * );
	AMateria *createMateria( std::string const & type);

};


#endif //PISCINE_CPP_MATERIASOURCE_HPP
