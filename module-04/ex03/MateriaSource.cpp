//
// Created by Antoine LANGLOIS on 2/15/22.
//

#include "MateriaSource.hpp"

//--------------------------- CONSTRUCTORS ---------------------------

MateriaSource::MateriaSource() {

	std::cout << "MateriaSource default constructor called" << std::endl;
	for (int i = 0; i < 4; i++)
		this->materias[i] = NULL;
	return ;
}

MateriaSource::~MateriaSource() {

	std::cout << "MateriaSource destructor called" << std::endl;
	for (int i = 0; i < 4; i++)
	{
		if (this->materias[i] != NULL)
			delete this->materias[i];
		this->materias[i] = NULL;
	}
	return ;
}

MateriaSource::MateriaSource( MateriaSource const & m ) {

	std::cout << "MateriaSource copy constructor called" << std::endl;
	for (int i = 0; i < 4; i++)
	{
		if (m.materias[i] == NULL)
			this->materias[i] = NULL;
		else
			this->materias[i] = m.materias[i]->clone();
	}
	return ;
}

//--------------------------- OPERATORS ------------------------------

MateriaSource &MateriaSource::operator=( MateriaSource const & m ) {

	std::cout << "MateriaSource assignation operator called" << std::endl;
	if (this != &m)
	{
		for (int i = 0; i < 4; i++)
		{
			if (m.materias[i] == NULL)
				this->materias[i] = NULL;
			else
				this->materias[i] = m.materias[i]->clone();
		}
	}
	return (*this);
}

//--------------------------- FUNCTIONS ------------------------------

void MateriaSource::learnMateria( AMateria * m ) {

	for (int i = 0; i < 4; i++)
	{
		if (this->materias[i] == NULL)
		{
			std::cout << "New Materia learned!" << std::endl;
			this->materias[i] = m->clone();
			return ;
		}
	}
	std::cout << "Error: Cannot learn new Materia" << std::endl;
	return ;
}

AMateria *MateriaSource::createMateria( std::string const & type ) {

	for (int i = 0; i < 4; i++)
	{
		if (this->materias[i] != NULL)
		{
			if (this->materias[i]->getType() == type)
			{
				std::cout << "Creating new Materia" << std::endl;
				return (this->materias[i]);
			}
		}
	}
	std::cout << "Error: Cannot create new Materia" << std::endl;
	return (NULL);
}