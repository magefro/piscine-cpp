//
// Created by Antoine LANGLOIS on 2/15/22.
//

#include "Cure.hpp"

//-------------------- CONSTRUCTORS ----------------------

Cure::Cure( Cure const & c ) : AMateria() {

	std::cout << "Cure copy constructor called" << std::endl;
	*this = c;
	return ;
}

Cure::Cure() : AMateria("cure") {

	std::cout << "Cure default constructor called" << std::endl;
	return ;
}

Cure::~Cure() {

	std::cout << "Cure destructor called" << std::endl;
	return ;
}

//-------------------- OPERATORS -------------------------

Cure &Cure::operator=( Cure const & c ) {

	std::cout << "Cure assignation operator called" << std::endl;
	if (this != &c)
	{
		this->type = c.getType();
	}
	return (*this);
}

//----------------- GETTERS AND SETTERS -----------------

std::string Cure::getType() const {

	return (this->type);
}

void Cure::setType( std::string type ) {

	this->type = type;
}

//---------------- INTERFACE ----------------------------

AMateria *Cure::clone() const {

	return (new Cure());
}

void Cure::use( ICharacter & target ) {

	std::cout << "* heals " << target.getName() << "'s wounds *" << std::endl;
	return ;
}