//
// Created by Antoine LANGLOIS on 2/15/22.
//

#ifndef PISCINE_CPP_IMATERIASOURCE_HPP
#define PISCINE_CPP_IMATERIASOURCE_HPP

#include "AMateria.hpp"

class IMateriaSource {

public:
	virtual ~IMateriaSource() {}
	virtual void learnMateria( AMateria* ) = 0;
	virtual AMateria * createMateria( std::string const & type ) = 0;

};


#endif //PISCINE_CPP_MATERIASOURCE_HPP
