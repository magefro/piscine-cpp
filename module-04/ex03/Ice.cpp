//
// Created by Antoine LANGLOIS on 2/15/22.
//

#include "Ice.hpp"

//------------------------- CONSTRUCTORS ----------------------

Ice::Ice( Ice const & i ) : AMateria() {

	std::cout << "Ice copy constructor called" << std::endl;
	*this = i;
	return ;
}

Ice::Ice() : AMateria("ice") {

	std::cout << "Ice default constructor called" << std::endl;
	return ;
}

Ice::~Ice() {

	std::cout << "Ice destructor called" << std::endl;
	return ;
}

//------------------------ OPERATORS ---------------------------

Ice &Ice::operator=( Ice const & i ) {

	std::cout << "Ice assignation operator called" << std::endl;
	if (this != &i)
	{
		this->type = i.getType();
	}
	return (*this);
}

//----------------------- GETTERS AND SETTERS ----------------

std::string Ice::getType() const {

	return (this->type);
}

void Ice::setType( std::string type ) {

	this->type = type;
}

//---------------------- INTERFACE --------------------------

AMateria *Ice::clone() const {

	return (new Ice());
}

void Ice::use( ICharacter & target ) {

	std::cout << "* shoots an ice bolt at " << target.getName() << " *" << std::endl;
	return ;
}