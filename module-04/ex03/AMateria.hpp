//
// Created by Antoine LANGLOIS on 2/15/22.
//

#ifndef PISCINE_CPP_AMATERIA_HPP
#define PISCINE_CPP_AMATERIA_HPP

#include <iostream>
#include "ICharacter.hpp"

class ICharacter;

class AMateria {

protected:
	std::string type;

public:
	AMateria( std::string const & type );
	AMateria( AMateria const & a );
	AMateria();
	virtual ~AMateria();

	AMateria &operator=( AMateria const & a );

	std::string const & getType() const;

	virtual AMateria *clone() const = 0;
	virtual void use( ICharacter & target );
};


#endif //PISCINE_CPP_AMATERIA_HPP
