//
// Created by Antoine LANGLOIS on 2/9/22.
//

#include "Brain.hpp"

//----------------------- CONSTRUCTORS ------------------

Brain::Brain( void ) {

	std::cout << "Brain default constructor called" << std::endl;
	return ;
}

Brain::~Brain( void ) {

	std::cout << "Brain destructor called" << std::endl;
	return ;
}

Brain::Brain( Brain const & b ) {

	std::cout << "Brain copy constructor called" << std::endl;
	*this = b;
	return ;
}

//---------------------- OPERATORS --------------------

Brain &Brain::operator=( Brain const & b ) {

	std::cout << "Brain assignation operator called" << std::endl;
	if (this != &b)
	{
		for (int i = 0; i < 100; i++)
		{
			std::cout << "Idea number " << i << " copied into new brain!" << std::endl;
			this->_ideas[i] = b._ideas[i];
		}

	}
	return (*this);
}