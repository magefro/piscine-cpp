//
// Created by Antoine LANGLOIS on 2/9/22.
//

#include "Animal.hpp"
#include "Dog.hpp"
#include "Cat.hpp"

int main() {

	int nb = 10;
	Animal *herd[nb];

	std::cout << "------- first test -------" << std::endl;

	for (int i = 0; i < nb; i++)
	{
		std::cout << "# " << i << ": ";
		if (i % 2 == 1)
			herd[i] = new Cat();
		else
			herd[i] = new Dog();
	}
	for (int j = 0; j < nb; j++)
	{
		std::cout << "# " << j << ": ";
		herd[j]->makeSound();
	}
	for (int k = 0; k < nb; k++)
	{
		std::cout << "# " << k << ": ";
		delete herd[k];
	}

	std::cout << "------- second test -------" << std::endl;

	std::cout << "snoopy : " << std::endl;
	const Animal *snoopy = new Dog();
	delete snoopy;

	std::cout << "miaouss : " << std::endl;
	const Animal *miaouss = new Cat();
	delete miaouss;

	std::cout << "rex : " << std::endl;
	const Dog rex;

	std::cout << "anonymous : " << std::endl;
	const Animal *anonymous = new Dog(rex);
	delete anonymous;

	return (0);
}