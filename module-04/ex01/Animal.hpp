//
// Created by antoine on 07/02/2022.
//

#ifndef PISCINE_CPP_ANIMAL_HPP
#define PISCINE_CPP_ANIMAL_HPP

#include <iostream>

class Animal {

protected:
	std::string type;

public:
	Animal( void );
	virtual ~Animal( void );
	Animal( Animal const & a );
	Animal( std::string type );

	Animal &operator=( Animal const & a );

	virtual void makeSound( void ) const ;

	std::string getType( void ) const;
	void 		setType( std::string const type );
};


#endif //PISCINE_CPP_ANIMAL_HPP
