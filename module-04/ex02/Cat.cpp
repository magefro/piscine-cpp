//
// Created by antoine on 07/02/2022.
//

#include "Cat.hpp"

//------------------- CONSTRUCTORS ----------------

Cat::Cat( void ) : AAnimal() {

	std::cout << "Cat default constructor called" << std::endl;
	this->setType("Cat");
	this->_brain = new Brain();
	return ;
}

Cat::~Cat( void ) {

	std::cout << "Cat destructor called" << std::endl;
	delete this->_brain;
	return ;
}

Cat::Cat( Cat const & c ) : AAnimal ( c ) {

	std::cout << "Cat copy constructor called" << std::endl;
	*this = c;
	return ;
}

//---------------- OPERATORS ---------------------

Cat &Cat::operator=( Cat const & c ) {

	std::cout << "Cat assignation operator called" << std::endl;
	if (this != &c)
	{
		this->type = c.getType();
		this->_brain = new Brain;
		*(this->_brain) = *(c._brain);
	}
	return (*this);
}

//--------------- FUNCTIONS ---------------------

void Cat::makeSound( void ) const {

	std::cout << "Miaou miaou mimimi" << std::endl;
	return ;
}