//
// Created by antoine on 07/02/2022.
//

#ifndef PISCINE_CPP_ANIMAL_HPP
#define PISCINE_CPP_ANIMAL_HPP

#include <iostream>

class AAnimal {

protected:
	std::string type;

public:
	AAnimal( void );
	virtual ~AAnimal( void );
	AAnimal( AAnimal const & a );
	AAnimal( std::string type );

	AAnimal &operator=( AAnimal const & a );

	virtual void makeSound( void ) const = 0;

	std::string getType( void ) const;
	void 		setType( std::string const type );
};


#endif //PISCINE_CPP_ANIMAL_HPP
