//
// Created by antoine on 07/02/2022.
//

#include "Dog.hpp"

//----------------- CONSTRUCTORS --------------------

Dog::Dog( void ) : AAnimal() {

	std::cout << "Dog default constructor called" << std::endl;
	this->setType("Dog");
	this->_brain = new Brain();
	return ;
}

Dog::~Dog( void ) {

	std::cout << "Dog destructor called" << std::endl;
	delete this->_brain;
	return ;
}

Dog::Dog( Dog const & d ) : AAnimal( d ) {

	std::cout << "Dog copy constructor called" << std::endl;
	*this = d;
	return ;
}

//---------------- OPERATORS ---------------------

Dog &Dog::operator=( Dog const & d ) {

	std::cout << "Dog assignation operator called" << std::endl;
	if (this != &d)
	{
		this->type = d.getType();
		this->_brain = new Brain;
		*(this->_brain) = *(d._brain);
	}
	return (*this);
}

//--------------- FUNCTIONS ----------------------

void Dog::makeSound( void ) const {

	std::cout << "Ouaf ouaf grrrrrr" << std::endl;
	return ;
}