//
// Created by antoine on 07/02/2022.
//

#ifndef PISCINE_CPP_CAT_HPP
#define PISCINE_CPP_CAT_HPP

#include <iostream>
#include "AAnimal.hpp"
#include "Brain.hpp"

class Cat : public AAnimal {

private:
	Brain *_brain;

public:
	Cat( void );
	~Cat( void );
	Cat( Cat const & c );

	Cat &operator=( Cat const & c );

	void makeSound( void ) const;

};


#endif //PISCINE_CPP_CAT_HPP
