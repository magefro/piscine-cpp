//
// Created by antoine on 07/02/2022.
//

#include "AAnimal.hpp"

//------------------ CONSTRUCTORS -------------------------

AAnimal::AAnimal( void ) : type("Default") {

	std::cout << "Animal default constructor called" << std::endl;
	return ;
}

AAnimal::~AAnimal( void ) {

	std::cout << "Animal destructor called" << std::endl;
	return ;
}
AAnimal::AAnimal( AAnimal const & a ) {

	std::cout << "Animal copy constructor called" << std::endl;
	*this = a;
	return ;
}

AAnimal::AAnimal( std::string type ) : type(type) {

	std::cout << "Animal constructor called" << std::endl;
	return ;
}

//------------------ OPERATORS -------------------------

AAnimal &AAnimal::operator=( AAnimal const & a ) {

	std::cout << "Animal assignation operator called" << std::endl;
	if (this != &a)
		this->type = a.getType();
	return (*this);
}

//------------------------ ACTIONS ----------------------------

void AAnimal::makeSound( void ) const {

	std::cout << "Thats the sound of the animal whoop whoop" << std::endl;
	return ;
}

//------------------ GETTERS / SETTERS ---------------------

std::string AAnimal::getType( void ) const {

	return (this->type);
}

void AAnimal::setType( std::string const type ) {

	this->type = type;
	return ;
}