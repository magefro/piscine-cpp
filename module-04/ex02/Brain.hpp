//
// Created by Antoine LANGLOIS on 2/9/22.
//

#ifndef PISCINE_CPP_BRAIN_HPP
#define PISCINE_CPP_BRAIN_HPP

#include <iostream>

class Brain {

private:
	std::string _ideas[100];

public:
	Brain( void );
	~Brain( void );
	Brain( Brain const & b );

	Brain &operator=( Brain const & b );
};


#endif //PISCINE_CPP_BRAIN_HPP
