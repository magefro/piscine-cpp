//
// Created by Antoine LANGLOIS on 2/9/22.
//

#include "AAnimal.hpp"
#include "Dog.hpp"
#include "Cat.hpp"

int main() {

	std::cout << "ok: " << std::endl;
	const Dog snoopy;

	std::cout << "ok: " << std::endl;
	const Cat miaouss;

	std::cout << "ok: " << std::endl;
	const AAnimal *anonymous = new Dog();
	delete anonymous;

	//std::cout << " error : " << std::endl;
	//const AAnimal *abstract = new AAnimal();
	//delete abstract;

	return (0);
}