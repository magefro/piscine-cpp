//
// Created by Antoine LANGLOIS on 2/15/22.
//

#include "Bureaucrat.hpp"

int main() {

	std::cout << "Creating bureaucrats with wrong grades:" << std::endl;

	try {

		Bureaucrat bubu_0(0, "toto");
	}
	catch (const std::exception & e) {

		std::cout << e.what() << std::endl;
	}

	try {

		Bureaucrat bubu_1(160, "tata");
	}
	catch (const std::exception & e) {

		std::cout << e.what() << std::endl;
	}

	std::cout << std::endl << "Creating bureaucrats then incrementing and decrementing grades:" << std::endl;

	Bureaucrat bubu_2(3, "titi");
	Bureaucrat bubu_3(148, "tutu");

	try {

		bubu_2.incrementGrade();
		bubu_2.incrementGrade();
		bubu_2.incrementGrade();
	}
	catch (const std::exception & e) {

		std::cout << e.what() << std::endl;
	}

	try {

		bubu_3.decrementGrade();
		bubu_3.decrementGrade();
		bubu_3.decrementGrade();
	}
	catch (const std::exception & e) {

		std::cout << e.what() << std::endl;
	}

	std::cout << std::endl << "Trying ofstream operator:" << std::endl;

	std::cout << bubu_2 << " / " << bubu_3 << " " << std::endl;

	std::cout << std::endl;

	return (0);
}