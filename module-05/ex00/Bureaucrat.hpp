//
// Created by Antoine LANGLOIS on 2/15/22.
//

#ifndef PISCINE_CPP_BUREAUCRAT_HPP
#define PISCINE_CPP_BUREAUCRAT_HPP

#include <iostream>

class Bureaucrat {

private:
	std::string	_name;
	int			_grade;

public:
	Bureaucrat( int grade, std::string name );
	Bureaucrat();
	~Bureaucrat();
	Bureaucrat( Bureaucrat const & b );

	Bureaucrat	&operator=( Bureaucrat const & b );

	std::string getName() const;
	int 		getGrade() const;

	void		checkGrade() const;

	void		incrementGrade();
	void		decrementGrade();

	class GradeTooHighException : public std::exception {
		public:
			virtual const char * what() const throw() {
				return ("Grade too high");
			}
	};
	class GradeTooLowException : public std::exception {
		public:
			virtual const char * what() const throw() {
				return ("Grade too low");
			}
	};

};

std::ostream &operator<<( std::ostream & o, Bureaucrat const & b );


#endif //PISCINE_CPP_BUREAUCRAT_HPP
