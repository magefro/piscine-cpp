//
// Created by Antoine LANGLOIS on 2/16/22.
//

#include "PresidentialPardonForm.hpp"

//------------------------- CONSTRUCTORS ----------------------------------

PresidentialPardonForm::PresidentialPardonForm() : Form("PresidentialPardonForm", 25, 5), _target("Target") {

	std::cout << "PresidentialPardonForm default constructor called" << std::endl;
	return ;
}

PresidentialPardonForm::~PresidentialPardonForm() {

	std::cout << "PresidentialPardonForm destructor called" << std::endl;
	return ;
}

PresidentialPardonForm::PresidentialPardonForm( PresidentialPardonForm const & pdf ) : Form("PresidentialPardonForm", 25, 5), _target(pdf.getTarget()) {

	std::cout << "PresidentialPardonForm copy constructor called" << std::endl;
	return ;
}

PresidentialPardonForm::PresidentialPardonForm( std::string target ) : Form("PresidentialPardonForm", 25, 5), _target(target) {

	std::cout << "PresidentialPardonForm constructor called" << std::endl;
	return ;
}

//------------------------ OPERATORS --------------------------------------

PresidentialPardonForm &PresidentialPardonForm::operator=( PresidentialPardonForm const & pdf ) {

	std::cout << "PresidentialPardonForm default constructor called" << std::endl;
	if (this != &pdf)
	{
		this->_target = pdf.getTarget();
		Form::operator=(pdf);
	}
	return (*this);
}

//----------------------- GETTERS --------------------------------------

std::string PresidentialPardonForm::getTarget() const {

	return (this->_target);
}

//------------------------ FUNCTIONS ------------------------------------

void 		PresidentialPardonForm::execute( Bureaucrat const & executor ) const {

	try {

		this->checkExecute(executor);
	}
	catch ( std::exception& e )
	{
		std::cout << executor.getName() << " could not execute "
				 << this->getName() << " because " << e.what() << std::endl;
		throw Form::GradeTooLowToExecuteException();
		return ;
	}
	std::cout << this->_target << " was forgiven by Zafod Beeblebrox"
			  << std::endl;
}