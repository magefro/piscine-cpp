//
// Created by Antoine LANGLOIS on 2/16/22.
//

#include <fstream>
#include "ShrubberyCreationForm.hpp"

//-------------------------------- CONSTRUCTORS ------------------------------------

ShrubberyCreationForm::ShrubberyCreationForm() : Form("ShrubberyCreationForm", 145, 137), _target("Target") {

	std::cout << "ShrubberyCreationForm default constructor called" << std::endl;
	return ;
}

ShrubberyCreationForm::~ShrubberyCreationForm() {

	std::cout << "ShrubberyCreationForm destructor called" << std::endl;
	return ;
}

ShrubberyCreationForm::ShrubberyCreationForm( ShrubberyCreationForm const & scf ) : Form("ShrubberyCreationForm", 145, 137), _target(scf.getTarget()) {

	std::cout << "ShrubberyCreationForm copy constructor called" << std::endl;
	*this = scf;
	return ;
}

ShrubberyCreationForm::ShrubberyCreationForm( std::string target ) : Form("ShrubberyCreationForm", 145, 137), _target(target) {

	std::cout << "ShrubberyCreationForm constructor called" << std::endl;
	return ;
}

//------------------------------- OPERATORS ----------------------------------------

ShrubberyCreationForm &ShrubberyCreationForm::operator=( ShrubberyCreationForm const & scf ) {

	std::cout << "ShrubberyCreationForm assignation operator called" << std::endl;
	if (this != &scf)
	{
		this->_target = scf.getTarget();
		Form::operator=(scf);
	}
	return (*this);
}

//----------------------- GETTERS --------------------------------------

std::string ShrubberyCreationForm::getTarget() const {

	return (this->_target);
}

//------------------------ FUNCTIONS -------------------------------------

void 		ShrubberyCreationForm::execute( Bureaucrat const & executor ) const {

	try {

		this->checkExecute(executor);
	}
	catch ( std::exception& e )
	{
		std::cout << executor.getName() << " could not execute "
				 << this->getName() << " because " << e.what() << std::endl;
		throw Form::GradeTooLowToExecuteException();
		return ;
	}
	std::string input_file = "ShrubberyTree";
	std::string output_file = this->_target + "_shrubbery";
	std::ifstream input(input_file.c_str());
	std::ofstream output(output_file.c_str());

	output << input.rdbuf();

	input.close();
	output.close();

	std::cout << output_file << " created" << std::endl;
}