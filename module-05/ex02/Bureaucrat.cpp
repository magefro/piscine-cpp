//
// Created by Antoine LANGLOIS on 2/15/22.
//

#include "Bureaucrat.hpp"

//--------------------------- CONSTRUCTORS ------------------------------

Bureaucrat::Bureaucrat( int grade, std::string name ) : _name(name), _grade(grade) {

	std::cout << "Bureaucrat constructor called" << std::endl;
	this->checkGrade();
	return;
}

Bureaucrat::Bureaucrat() : _name("Default"), _grade(150) {

	std::cout << "Bureaucrat default constructor called" << std::endl;
	this->checkGrade();
	return ;
}

Bureaucrat::~Bureaucrat() {

	std::cout << "Bureaucrat destructor called" << std::endl;
	return ;
}

Bureaucrat::Bureaucrat( Bureaucrat const & b ) {

	std::cout << "Bureaucrat copy constructor called" << std::endl;
	this->_name = b.getName();
	this->_grade = b.getGrade();
	this->checkGrade();
	return ;
}

//-------------------------- OPERATORS ---------------------------------

Bureaucrat	&Bureaucrat::operator=( Bureaucrat const & b ) {

	std::cout << "Bureaucrat assignation operator called" << std::endl;
	if (this != &b)
	{
		this->_name = b.getName();
		this->_grade = b.getGrade();
	}
	return (*this);
}

std::ostream &operator<<( std::ostream & o, Bureaucrat const & b ) {

	o << b.getName() << ", bureaucrat grade " << b.getGrade();
	return (o);
}

//--------------------- GETTERS / SETTERS ------------------------------

std::string Bureaucrat::getName() const {

	return (this->_name);
}

int 		Bureaucrat::getGrade() const {

	return (this->_grade);
}

//---------------------- FUNCTIONS -----------------------------------

void		Bureaucrat::incrementGrade() {

	if (this->_grade > 1)
	{
		std::cout << "Incrementing grade of " << this->getName() << std::endl;
		this->_grade--;
	}
	else
		throw Bureaucrat::GradeTooHighException();
	return ;
}

void		Bureaucrat::decrementGrade() {

	if (this->_grade < 150)
	{
		std::cout << "Decrementing grade of " << this->getName() << std::endl;
		this->_grade++;
	}
	else
		throw Bureaucrat::GradeTooLowException();
	return ;
}

void		Bureaucrat::signForm( Form & f ) {

	if (f.getSigned())
	{
		std::cout << "Form " << f.getName() << " is already signed." << std::endl;
		return ;
	}
	try {

		f.beSigned(*this);
	}
	catch ( std::exception& e )
	{
		std::cout << this->getName() << " cannot sign form "
				  << f.getName() << " because " << e.what() << std::endl;
		return ;
	}
	std::cout << this->getName() << " signed " << f.getName() << std::endl;
}

void		Bureaucrat::executeForm( Form const & f ) {

	try {

		f.execute(*this);
	}
	catch ( std::exception& e )
	{
		std::cout << f.getName() << " was not executed by "
				  << this->getName() << " because of "
				  << e.what() << std::endl;
		return ;
	}
	std::cout << this->getName() << " executes "
			  << f.getName() << std::endl;
}

//----------------------- CHECKERS ------------------------------------

void Bureaucrat::checkGrade() const {

	if (this->getGrade() > 150)
		throw Bureaucrat::GradeTooLowException();
	if (this->getGrade() < 1)
		throw Bureaucrat::GradeTooHighException();
	return ;
}