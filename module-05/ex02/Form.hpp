//
// Created by Antoine LANGLOIS on 2/16/22.
//

#ifndef PISCINE_CPP_FORM_HPP
#define PISCINE_CPP_FORM_HPP

#include "Bureaucrat.hpp"

class Bureaucrat;

class Form {

private:
	std::string const	_name;
	int 				_gradeSign;
	int 				_gradeExec;
	bool 				_signed;

public:
	Form();
	virtual ~Form();
	Form( Form const & f );
	Form( std::string name, int gradeSign, int gradeExec );

	Form				&operator=( Form const & f );

	std::string			getName() const;
	int 				getGradeSign() const;
	int 				getGradeExec() const;
	bool 				getSigned() const;

	void				checkGrade() const;
	void 				checkExecute( Bureaucrat const & b ) const;

	void 				beSigned( Bureaucrat const & b );

	virtual void 		execute( Bureaucrat const & executor ) const = 0;

	class GradeTooHighException : public std::exception {
		public:
			virtual const char * what() const throw() {
				return ("grade is too high");
			}
	};

	class GradeTooLowException : public std::exception {
		public:
			virtual const char * what() const throw() {
				return ("grade is too low");
			}
	};

	class NotSignedException : public std::exception {
	public:
		virtual const char * what() const throw() {
			return ("form is not signed");
		}
	};

	class GradeTooLowToExecuteException : public std::exception {
	public:
		virtual const char * what() const throw() {
			return ("grade is too low to execute");
		}
	};
};

std::ostream		&operator<<( std::ostream & o, Form const & f );

#endif //PISCINE_CPP_FORM_HPP
