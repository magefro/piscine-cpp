//
// Created by Antoine LANGLOIS on 2/16/22.
//

#include "Form.hpp"
#include "Bureaucrat.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"

int main() {

	std::cout << "Creating many bureaucrats:" << std::endl;
	Bureaucrat bubu_1(150, "bubu_1");
	Bureaucrat bubu_2(144, "bubu_2");
	Bureaucrat bubu_3(136, "bubu_3");
	Bureaucrat bubu_4(71, "bubu_4");
	Bureaucrat bubu_5(44, "bubu_5");
	Bureaucrat bubu_6(24, "bubu_6");
	Bureaucrat bubu_7(4, "bubu_7");

	std::cout << std::endl << "Creating and testing signing ShrubberyCreationForm:" << std::endl;
	Form *shrubbery = new ShrubberyCreationForm("pizza");
	bubu_1.signForm(*shrubbery);
	bubu_2.signForm(*shrubbery);

	std::cout << std::endl << "Testing executing ShrubberyCreationForm:" << std::endl;
	bubu_2.executeForm(*shrubbery);
	bubu_3.executeForm(*shrubbery);

	std::cout << std::endl << "Creating and testing signing RobotomyRequestForm:" << std::endl;
	Form *robotomy = new RobotomyRequestForm("couscous");
	bubu_3.signForm(*robotomy);
	bubu_4.signForm(*robotomy);

	std::cout << std::endl << "Testing executing RobotomyRequestForm:" << std::endl;
	bubu_4.executeForm(*robotomy);
	bubu_5.executeForm(*robotomy);
	bubu_5.executeForm(*robotomy);
	bubu_5.executeForm(*robotomy);
	bubu_5.executeForm(*robotomy);
	bubu_5.executeForm(*robotomy);

	std::cout << std::endl << "Creating and testing signing PresidentialPardonForm:" << std::endl;
	Form *presidential = new PresidentialPardonForm("burger");
	bubu_5.signForm(*presidential);
	bubu_6.signForm(*presidential);

	std::cout << std::endl << "Testing executing PresidentialPardonForm:" << std::endl;
	bubu_6.executeForm(*presidential);
	bubu_7.executeForm(*presidential);

	std::cout << std::endl;

	delete shrubbery;
	delete robotomy;
	delete presidential;

	return (0);
}