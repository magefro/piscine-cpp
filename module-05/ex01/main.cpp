//
// Created by Antoine LANGLOIS on 2/16/22.
//

#include "Form.hpp"
#include "Bureaucrat.hpp"

int main() {

	Bureaucrat bubu_1(10, "bubu");

	Form form_1("form_1", 10, 10);
	Form form_2("form_2", 11, 11);
	Form form_3("form_3", 9, 9);

	std::cout << std::endl << "Bureaucrat of grade 10 sign form 10, 11 and 9:" << std::endl;
	bubu_1.signForm(form_1);
	bubu_1.signForm(form_2);
	bubu_1.signForm(form_3);

	std::cout << std::endl << "Trying to sign already signed form:" << std::endl;
	bubu_1.signForm(form_1);

	std::cout << std::endl;

	std::cout << form_1 << std::endl
			  << form_2 << std::endl
			  << form_3 << std::endl;

	return (0);
}