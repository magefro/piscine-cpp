//
// Created by Antoine LANGLOIS on 2/16/22.
//

#ifndef PISCINE_CPP_FORM_HPP
#define PISCINE_CPP_FORM_HPP

#include "Bureaucrat.hpp"

class Bureaucrat;

class Form {

private:
	std::string const	_name;
	int 				_gradeSign;
	int 				_gradeExec;
	bool 				_signed;

public:
	Form();
	~Form();
	Form( Form const & f );
	Form( std::string name, int gradeSign, int gradeExec );

	Form				&operator=( Form const & f );

	std::string			getName() const;
	int 				getGradeSign() const;
	int 				getGradeExec() const;
	bool 				getSigned() const;

	void				checkGrade() const;

	void 				beSigned( Bureaucrat const & b );

	class GradeTooHighException : public std::exception {
		public:
			virtual const char * what() const throw() {
				return ("grade is too high");
			}
	};

	class GradeTooLowException : public std::exception {
		public:
			virtual const char * what() const throw() {
				return ("grade is too low");
			}
	};
};

std::ostream		&operator<<( std::ostream & o, Form const & f );

#endif //PISCINE_CPP_FORM_HPP
