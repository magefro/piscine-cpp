//
// Created by Antoine LANGLOIS on 2/16/22.
//

#include "RobotomyRequestForm.hpp"

//--------------------------- CONSTRUCTORS ------------------------------

RobotomyRequestForm::RobotomyRequestForm() : Form("RobotomyRequestForm", 72, 45), _target("Target") {

	std::cout << "RobotomyRequestForm default constructor called" << std::endl;
	return ;
}

RobotomyRequestForm::~RobotomyRequestForm() {

	std::cout << "RobotomyRequestForm destructor called" << std::endl;
	return ;
}

RobotomyRequestForm::RobotomyRequestForm( RobotomyRequestForm const & rrf ) : Form("RobotomyRequestForm", 72, 45), _target(rrf.getTarget()) {

	std::cout << "RobotomyRequestForm copy constructor called" << std::endl;
	*this = rrf;
	return ;
}

RobotomyRequestForm::RobotomyRequestForm( std::string target ) : Form("RobotomyRequestForm", 72, 45), _target(target) {

	std::cout << "RobotomyRequestForm constructor called" << std::endl;
	return ;
}

//-------------------------- OPERATORS ---------------------------------

RobotomyRequestForm &RobotomyRequestForm::operator=( RobotomyRequestForm const & rrf ) {

	std::cout << "RobotomyRequestForm assignation operator called" << std::endl;
	if (this != &rrf)
	{
		this->_target = rrf.getTarget();
		Form::operator=(rrf);
	}
	return (*this);
}

//----------------------- GETTERS --------------------------------------

std::string RobotomyRequestForm::getTarget() const {

	return (this->_target);
}

//------------------------ FUNCTIONS ------------------------------------

void 		RobotomyRequestForm::execute( Bureaucrat const & executor ) const {

	try {

		this->checkExecute(executor);
	}
	catch ( std::exception& e )
	{
		std::cout << executor.getName() << " could not execute "
				 << this->getName() << " because " << e.what() << std::endl;
		throw Form::GradeTooLowToExecuteException();
		return ;
	}
	std::cout << "* some drilling noises *" << std::endl
			  << this->_target;
	if (std::rand() % 2 == 1)
		std::cout << " has been robotomized successfully.";
	else
		std::cout << " has not been robotomized.";
	std::cout << std::endl;
}