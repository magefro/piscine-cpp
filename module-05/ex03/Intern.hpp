//
// Created by Antoine LANGLOIS on 2/16/22.
//

#ifndef PISCINE_CPP_INTERN_HPP
#define PISCINE_CPP_INTERN_HPP

#include "Form.hpp"

class Intern {

public:
	Intern();
	~Intern();
	Intern( Intern const & i );

	Intern &operator=( Intern const & i );

	Form	*makeForm( std::string name, std::string target );

private:
	class CantCreateFormException : public std::exception {
		public:
			virtual const char * what() const throw() {
				return ("Intern could not create form.");
			}
	};
};


#endif //PISCINE_CPP_INTERN_HPP
