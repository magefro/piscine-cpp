//
// Created by Antoine LANGLOIS on 2/16/22.
//

#ifndef PISCINE_CPP_PRESIDENTIALPARDONFORM_HPP
#define PISCINE_CPP_PRESIDENTIALPARDONFORM_HPP

#include "Form.hpp"
#include "Bureaucrat.hpp"

class PresidentialPardonForm : public Form {

private:
	std::string _target;

public:
	PresidentialPardonForm();
	~PresidentialPardonForm();
	PresidentialPardonForm( PresidentialPardonForm const & pdf );
	PresidentialPardonForm( std::string target );

	PresidentialPardonForm &operator=( PresidentialPardonForm const & pdf );

	std::string getTarget() const;

	void 		execute( Bureaucrat const & executor ) const;

};


#endif //PISCINE_CPP_PRESIDENTIALPARDONFORM_HPP
