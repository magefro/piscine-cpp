//
// Created by Antoine LANGLOIS on 2/15/22.
//

#ifndef PISCINE_CPP_BUREAUCRAT_HPP
#define PISCINE_CPP_BUREAUCRAT_HPP

#include <iostream>
#include "Form.hpp"

class Form;

class Bureaucrat {

private:
	std::string	_name;
	int			_grade;

public:
	Bureaucrat( int grade, std::string name );
	Bureaucrat();
	~Bureaucrat();
	Bureaucrat( Bureaucrat const & b );

	Bureaucrat	&operator=( Bureaucrat const & b );

	std::string getName() const;
	int 		getGrade() const;

	void		checkGrade() const;

	void		incrementGrade();
	void		decrementGrade();

	void		signForm( Form & f );
	void		executeForm( Form const & f );

	class GradeTooHighException : public std::exception {
		public:
			virtual const char * what() const throw() {
				return ("grade is too high");
			}
	};
	class GradeTooLowException : public std::exception {
		public:
			virtual const char * what() const throw() {
				return ("grade is too low");
			}
	};

};

std::ostream	&operator<<( std::ostream & o, Bureaucrat const & b );


#endif //PISCINE_CPP_BUREAUCRAT_HPP
