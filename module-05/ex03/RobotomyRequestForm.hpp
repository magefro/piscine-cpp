//
// Created by Antoine LANGLOIS on 2/16/22.
//

#ifndef PISCINE_CPP_ROBOTOMYREQUESTFORM_HPP
#define PISCINE_CPP_ROBOTOMYREQUESTFORM_HPP

#include "Form.hpp"
#include "Bureaucrat.hpp"

class RobotomyRequestForm : public Form {

private:
	std::string _target;

public:
	RobotomyRequestForm();
	~RobotomyRequestForm();
	RobotomyRequestForm( RobotomyRequestForm const & rrf );
	RobotomyRequestForm( std::string target );

	RobotomyRequestForm &operator=( RobotomyRequestForm const & rrf );

	std::string getTarget() const;

	void 		execute( Bureaucrat const & executor ) const;

};


#endif //PISCINE_CPP_ROBOTOMYREQUESTFORM_HPP
