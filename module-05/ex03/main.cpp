//
// Created by Antoine LANGLOIS on 2/16/22.
//

#include "Intern.hpp"
#include "Form.hpp"
#include "Bureaucrat.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"

int main() {

	std::cout << "Creating some intern:" << std::endl;
	Intern boloss;

	Form *scf;
	Form *rrf;
	Form *pdf;
	Form *qlf;

	std::cout << std::endl << "Creating different forms:" << std::endl;
	scf = boloss.makeForm("shrubbery creation", "blablu");
	std::cout << std::endl;
	rrf = boloss.makeForm("robotomy request", "blibli");
	std::cout << std::endl;
	pdf = boloss.makeForm("presidential pardon", "bloubla");
	std::cout << std::endl;
	qlf = boloss.makeForm("mort aux vaches", "flipper le dauphin");

	std::cout << std::endl;

	delete scf;
	delete rrf;
	delete pdf;

	return (0);

}