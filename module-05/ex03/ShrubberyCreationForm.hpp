//
// Created by Antoine LANGLOIS on 2/16/22.
//

#ifndef PISCINE_CPP_SHRUBBERYCREATIONFORM_HPP
#define PISCINE_CPP_SHRUBBERYCREATIONFORM_HPP

#include "Form.hpp"
#include "Bureaucrat.hpp"

class ShrubberyCreationForm : public Form {

private:
	std::string _target;

public:
	ShrubberyCreationForm();
	~ShrubberyCreationForm();
	ShrubberyCreationForm( ShrubberyCreationForm const & scf );
	ShrubberyCreationForm( std::string target );

	ShrubberyCreationForm &operator=( ShrubberyCreationForm const & scf );

	std::string getTarget() const;

	void 		execute( Bureaucrat const & executor ) const;

};


#endif //PISCINE_CPP_SHRUBBERYCREATIONFORM_HPP
