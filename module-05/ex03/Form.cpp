//
// Created by Antoine LANGLOIS on 2/16/22.
//

#include "Form.hpp"

//----------------------------------- CONSTRUCTORS ------------------------------------

Form::Form() : _name("Form"), _gradeSign(150), _gradeExec(150), _signed(false) {

	std::cout << "Form default constructor called" << std::endl;
	this->checkGrade();
	return ;
}

Form::~Form() {

	std::cout << "Form destructor called" << std::endl;
	return ;
}

Form::Form( Form const & f ) : _name(f.getName()), _gradeSign(f.getGradeSign()), _gradeExec(f.getGradeExec()), _signed(f.getSigned()) {

	std::cout << "Form copy constructor called" << std::endl;
	this->checkGrade();
	return ;
}

Form::Form( std::string name, int gradeSign, int gradeExec ) : _name(name), _gradeSign(gradeSign), _gradeExec(gradeExec), _signed(false) {

	std::cout << "Form constructor called" << std::endl;
	this->checkGrade();
	return ;
}

//------------------------------- OPERATORS ----------------------------------

Form &Form::operator=( Form const & f ) {

	std::cout << "Form assignation operator called" << std::endl;
	if (this != &f)
	{
		//this->_name = f.getName();
		this->_gradeSign = f.getGradeSign();
		this->_gradeExec = f.getGradeExec();
		this->_signed = f.getSigned();
	}
	return (*this);
}

std::ostream &operator<<( std::ostream & o, Form const & f ) {

	o << "Form name:       " << f.getName() << std::endl
	  << "Form grade sign: " << f.getGradeSign() << std::endl
	  << "Form garde exec: " << f.getGradeExec() << std::endl
	  << "Form is signed:  " << f.getSigned() << std::endl;
	return (o);
}

//------------------------ GETTERS --------------------------------------------

std::string			Form::getName() const {

	return (this->_name);
}

int 				Form::getGradeSign() const {

	return (this->_gradeSign);
}

int 				Form::getGradeExec() const {

	return (this->_gradeExec);
}

bool 				Form::getSigned() const {

	return (this->_signed);
}

//----------------------------- FUNCTIONS -----------------------------------

void 				Form::beSigned( Bureaucrat const & b ) {

	if (this->getSigned())
		return ;
	if (b.getGrade() <= this->_gradeSign)
		this->_signed = true;
	else
		throw Form::GradeTooLowException();
}

//---------------------------- CHECKERS ------------------------------------

void				Form::checkGrade() const {

	if (this->getGradeSign() > 150)
		throw Bureaucrat::GradeTooLowException();
	if (this->getGradeSign() < 1)
		throw Bureaucrat::GradeTooHighException();
	if (this->getGradeExec() > 150)
		throw Bureaucrat::GradeTooLowException();
	if (this->getGradeExec() < 1)
		throw Bureaucrat::GradeTooHighException();
	return ;
}

void				Form::checkExecute( Bureaucrat const & b ) const {

	if (!this->getSigned())
		throw Form::NotSignedException();
	if (b.getGrade() > this->getGradeExec())
		throw Form::GradeTooLowToExecuteException();
	return ;
}