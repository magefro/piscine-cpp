//
// Created by Antoine LANGLOIS on 2/16/22.
//

#include "Form.hpp"
#include "Intern.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"

std::string forms[3] = {

		"shrubbery creation",
		"robotomy request",
		"presidential pardon",
};

//------------------------------- CONSTRUCTORS -----------------------------------

Intern::Intern() {

	std::cout << "Intern default constructor called" << std::endl;
	return ;
}

Intern::~Intern() {

	std::cout << "Intern destructor called" << std::endl;
	return ;
}

Intern::Intern( Intern const & i ) {

	std::cout << "Intern copy constructor called" << std::endl;
	*this = i;
	return ;
}

//------------------------------- OPERATORS -----------------------------------

Intern &Intern::operator=( Intern const & i ) {

	std::cout << "Intern assignation operator called" << std::endl;
	if (this != &i)
	{
		//------------------
	}
	return (*this);
}

//------------------------------- FUNCTIONS -----------------------------------

static Form *shrubbery(std::string target ) {

	return (new ShrubberyCreationForm(target));
}


static Form *robotomy( std::string target ) {

	return (new RobotomyRequestForm(target));
}


static Form *presidential( std::string target ) {

	return (new PresidentialPardonForm(target));
}


Form	*Intern::makeForm( std::string name, std::string target ) {

	Form* (*createForm[])( std::string target ) = {

			&shrubbery,
			&robotomy,
			&presidential,
	};

	for (int i = 0; i < 3; i++)
	{
		if (name == forms[i])
		{
			std::cout << "Intern creates " << name << std::endl;
			return (createForm[i](target));
		}
	}
	std::cout << "Intern could not create " << name << std::endl;
	return (NULL);
}