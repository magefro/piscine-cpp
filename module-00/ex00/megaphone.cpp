//
// Created by alangloi on 12/5/21.
//

#include <iostream>

int main(int argc, char **argv) {
	int num_arg;
	int index;

	if (argc == 1)
		std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *" << std::endl;
	num_arg = 1;
	while (argv[num_arg]) {
		index = 0;
		std::string str = argv[num_arg];
		while (str[index]) {
			std::cout << (char)toupper(str[index]);
			index++;
		}
		num_arg++;
	}
	std::cout << std::endl;
	return 0;
}