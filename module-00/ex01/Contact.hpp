//
// Created by alangloi on 12/5/21.
//

#include <iostream>
#include <string>

#ifndef PISCINE_CPP_CONTACT_CLASS_HPP
# define PISCINE_CPP_CONTACT_CLASS_HPP

class Contact {

private:
	int					_index;
	std::string			_first_name;
	std::string			_last_name;
	std::string			_nickname;
	std::string			_phone_number;
	std::string			_darkest_secret;

	static std::string	_convert_phone( std::string );
	static std::string	_cut( std::string input );

public:
	Contact( void );
	~Contact( void );

	void				setContact( int index );
	void				print_line( void ) const ;
	void				print_infos( void ) const ;

};

#endif
