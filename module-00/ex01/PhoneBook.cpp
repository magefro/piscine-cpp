#include <iostream>
#include <iomanip>
#include <string>
#include <cstdlib>
#include <cctype>
#include "PhoneBook.hpp"

int PhoneBook::_nbInst = 0;

PhoneBook::PhoneBook( void ) {
	return ;
}

PhoneBook::~PhoneBook( void ) {
	return ;
}

void PhoneBook::addContact( void ) {
	if (PhoneBook::_nbInst >= 7)
	{
		std::cout << "PhoneBook is full! Can't add contact anymore." << std::endl;
	}
	else
	{
		this->contacts[PhoneBook::_nbInst].setContact(PhoneBook::_nbInst);
		PhoneBook::_nbInst += 1;
	}
	return ;
}

void	PhoneBook::_print_all( void ) const {
	int i = 0;
	std::cout << "++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
	std::cout << "|     Index|First Name| Last Name|  Nickname|" << std::endl;
	while (i < PhoneBook::_nbInst)
	{
		this->contacts[i].print_line();
		i++;
	}
	std::cout << "++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
}

int PhoneBook::searchContact( void ) const {
	std::string input;

	PhoneBook::_print_all();
	std::cout << "Please enter id: ";
	std::getline(std::cin, input);
	if (input.length() > 1 || std::isdigit(input.at(0)) == 0) {
		std::cout << "Wrong id." << std::endl;
		return (0);
	}
	int i = std::atoi(input.c_str());
	if (i < 0 || i > PhoneBook::_nbInst)
	{
		std::cout << "Out of range." << std::endl;
		return (0);
	}
	this->contacts[i].print_infos();
	return (-1);
}
