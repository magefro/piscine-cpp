#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cctype>
#include "Contact.hpp"

Contact::Contact( void ) {
	return ;
}

Contact::~Contact( void ) {
	return ;
}

std::string	Contact::_cut( std::string input ) {
	if (input.length() > 10)
	{
		input.resize(9);
		input.replace(9, 1, ".");
	}
	return (input);
}

std::string 	Contact::_convert_phone( std::string input ) {
	int i = 0;

	if (input.at(i) == '+')
		i++;
	while (i < (int)input.length())
	{
		if (std::isdigit(input.at(i)))
			i++;
		else
		{
			std::cout << "Phone number not valid." << std::endl;
			return ("");
		}
	}
	return (input);
}

void	Contact::print_infos( void ) const {
	std::cout << "Index: " << this->_index << std::endl;
	std::cout << "First Name: " << this->_first_name << std::endl;
	std::cout << "Last Name: " << this->_last_name << std::endl;
	std::cout << "Nickname: " << this->_nickname << std::endl;
	std::cout << "Phone Number: " << this->_phone_number << std::endl;
	std::cout << "Darkest Secret: " << this->_darkest_secret << std::endl;
}

void	Contact::print_line( void ) const {
	std::string newString;

	std::cout << "|" << std::setw(10) << this->_index << "|";
	std::cout << std::setw(10) << this->_cut(this->_first_name) << "|";
	std::cout << std::setw(10) << this->_cut(this->_last_name) << "|";
	std::cout << std::setw(10) << this->_cut(this->_nickname) << "|" << std::endl;
}

void	Contact::setContact( int index ) {
	std::string input;

	this->_index = index;
	std::cout << "Adding new contact" << std::endl;
	std::cout << "First name: ";
	std::getline(std::cin, this->_first_name);
	std::cout << "Last name: ";
	std::getline(std::cin, this->_last_name);
	std::cout << "Nickname: ";
	std::getline(std::cin, this->_nickname);
	std::cout << "Phone Number: ";
	std::getline(std::cin, input);
	input = this->_convert_phone(input);
	if (input.size() > 0)
		this->_phone_number = input;
	std::cout << "Darkest Secret: ";
	std::getline(std::cin, this->_darkest_secret);
}
