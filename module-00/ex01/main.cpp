//
// Created by alangloi on 12/5/21.
//

#include <iostream>
#include <iomanip>
#include "PhoneBook.hpp"
#include "Contact.hpp"

int main() {
	PhoneBook phonebook;
	std::string command;
	bool run;

	run = true;
	std::cout << "❀❁✾ Welcome ❀❁✾" << std::endl;
	while (run)
	{
		std::cout << "> ";
		std::getline(std::cin, command);
		if (command == "ADD")
			phonebook.addContact();
		else if (command == "SEARCH")
			phonebook.searchContact();
		else if (command == "EXIT")
		{
			std::cout << "Bye ;)" << std::endl;
			run = false;
		}

	}
	return (0);
}
