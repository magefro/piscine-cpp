//
// Created by alangloi on 12/5/21.
//

#ifndef PISCINE_CPP_DATABASE_HPP
# define PISCINE_CPP_DATABASE_HPP

#include "Contact.hpp"

class PhoneBook {

private:
	static int	_nbInst;

	void		_print_all( void ) const ;

public:
	Contact		contacts[8];

	PhoneBook( void );
	~PhoneBook( void );

	void		addContact( void );
	int			searchContact( void ) const ;

};

#endif
