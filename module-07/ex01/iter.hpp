//
// Created by Antoine LANGLOIS on 2/28/22.
//

#ifndef PISCINE_CPP_ITER_HPP
#define PISCINE_CPP_ITER_HPP

#include <iostream>

template<typename T>
void 	iter( T ptr[], int const & size, void f( T const & a ) )
{
	for (int i = 0; ptr && i < size; i++)
	{
		f(ptr[i]);
	}
	return ;
}

#endif //PISCINE_CPP_ITER_HPP
