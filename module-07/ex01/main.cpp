//
// Created by Antoine LANGLOIS on 2/28/22.
//

#include "iter.hpp"

template<typename T>
void print( T const & a ) {

	std::cout << a << std::endl;
}

int main() {

	int				tab_1[5] = {1,2,3,4,5};
	std::string		tab_2[3] = {"salut","les","amis"};

	iter(tab_1, 5, print);
	iter(tab_2, 3, print);

	return (0);
}