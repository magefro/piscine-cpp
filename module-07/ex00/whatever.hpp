//
// Created by Antoine LANGLOIS on 2/28/22.
//

#ifndef PISCINE_CPP_WHATEVER_HPP
#define PISCINE_CPP_WHATEVER_HPP

#include <iostream>

template< typename T >
void	swap( T & A, T & B) {

	T tmp;

	tmp = A;
	A = B;
	B = tmp;

	return ;
}

template< typename T >
T const &min( T const & A, T const & B) {

	return (A > B ? B : A);
}

template< typename T >
T const &max( T const & A, T const & B) {

	return (A > B ? A : B);
}


#endif //PISCINE_CPP_STACK_HPP
