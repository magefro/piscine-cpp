//
// Created by Antoine LANGLOIS on 2/28/22.
//

#ifndef PISCINE_CPP_ARRAY_TPP
#define PISCINE_CPP_ARRAY_TPP

#include <iostream>

template< typename T>
class Array {

private:
	T *				_array;
	unsigned int	_size;

public:
	Array( void ) : _array(new T[0]), _size(0) {

		//std::cout << "Array template T default constructor called" << std::endl;
		return ;
	};

	~Array( void ) {

		if ( this->size() > 0 && this->_array != NULL)
			delete [] this->_array;
		//std::cout << "Array template T destructor called" << std::endl;
		return ;
	};

	Array( unsigned int n ) : _array(new T[n]), _size(n) {

		//std::cout << "Array template T constructor called" << std::endl;
		return ;
	};

	Array( Array const & a ) {

		this->_size = a.size();
		this->_array = new T[a.size()];
		for (unsigned int i = 0; i < a.size(); i++)
			this->_array[i] = a._array[i];
		//std::cout << "Array template T copy constructor called" << std::endl;
		return ;
	};

	Array &operator=( Array const & a ) {

		if (this != &a)
		{
			if ( this->size() > 0 && this->_array )
				delete [] this->_array;
			this->_size = a._size;
			this->_array = new T[a.size()];
			for (unsigned int i = 0; i < a.size(); i++)
				this->_array[i] = a._array[i];
		}
		//std::cout << "Array template T assignation operator called" << std::endl;
		return (*this);
	};

	T & operator[]( unsigned int const & index ) {

		if (index > this->size() || index < 0)
			throw Array::outOfRangeException();
		//std::cout << "Array template T brackets operator called" << std::endl;
		return (this->_array[index]);
	};

	unsigned int	size() const {

		return (this->_size);
	};

	class outOfRangeException : public std::exception {
		public:
			virtual const char * what() const throw() {
				return ("index out of range");
			}
	};
};

template<typename T>
std::ostream & operator<<( std::ostream & o, Array<T> & a ) {

	for (unsigned int i = 0; i < a.size(); i++) {
		o << a[i] << " ";
	}
	return (o);
}

#endif //PISCINE_CPP_ARRAY_TPP
