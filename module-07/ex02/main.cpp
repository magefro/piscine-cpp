//
// Created by Antoine LANGLOIS on 2/28/22.
//

#include "Array.tpp"

int main() {

	std::cout << "creating int and str array:" << std::endl;
	Array<int>			int_arr(10);
	Array<std::string>	str_arr(5);

	std::cout << std::endl << "printing int_arr and the 5th element of int arr:" << std::endl;
	std::cout << int_arr << std::endl;
	std::cout << int_arr[5] << std::endl;

	std::cout << std::endl << "assigning number of elements of str_arr:" << std::endl;
	for (int i = 0; i < 5; i++)
		str_arr[i] = "id_" + std::to_string(i);

	std::cout << std::endl << "creating copies of int_arr and str_arr:" << std::endl;
	Array<int>			new_int_arr(int_arr);
	Array<std::string>	new_str_arr = str_arr;

	std::cout << std::endl << "calling size functions:" << std::endl;
	std::cout << "int arr size:     " << int_arr.size() << std::endl;
	std::cout << "str arr size:     " << str_arr.size() << std::endl;
	std::cout << "new int arr size: " << new_int_arr.size() << std::endl;
	std::cout << "new str arr size: " << new_str_arr.size() << std::endl;

	std::cout << std::endl << "printing new_int_arr and new_str_arr:" << std::endl;
	std::cout << new_int_arr << std::endl;
	std::cout << new_str_arr << std::endl;

	std::cout << std::endl << "assigning int_arr[5] to 666 and str_arr[3] to blabla:" << std::endl;
	new_int_arr[5] = 666;
	new_str_arr[3] = "blabla";

	std::cout << std::endl << "printing int_arr:" << std::endl;
	std::cout << int_arr << std::endl;

	std::cout << std::endl << "printing str_arr:" << std::endl;
	std::cout << str_arr << std::endl;

	std::cout << std::endl << "printing new_int_arr:" << std::endl;
	std::cout << new_int_arr << std::endl;

	std::cout << std::endl << "printing new_str_arr:" << std::endl;
	std::cout << new_str_arr << std::endl;

	std::cout << std::endl << "accessing elements of new_str_arr (should return index out of range to the end):" << std::endl;
	try {
		for (int i = 0; i < 6; i++)
		{
			std::cout << new_str_arr[i] << std::endl;
		}
		std::cout << new_str_arr[100] << std::endl;
	} catch ( std::exception & e ) {

		std::cout << e.what() << std::endl;
	}

	std::cout << std::endl << "create empty array and try to display it:" << std::endl;

	Array<int> arr;
	std::cout << "[" << arr << "]" << std::endl;

	int * a = new int();
	std::cout << *a << std::endl;

	std::cout << std::endl;

	delete a;

	return (0);
}