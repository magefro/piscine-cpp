//
// Created by alangloi on 1/27/22.
//

#include "ScavTrap.hpp"
#include "ClapTrap.hpp"

//----------------------- CONSTRUCTORS -----------------------------------------------

ScavTrap::ScavTrap( std::string name) : ClapTrap(name, 100, 50, 20) {

	std::cout << "ScavTrap constructor called" << std::endl;
	return ;
}

ScavTrap::ScavTrap( void ) : ClapTrap("Default", 100, 50, 20) {

	std::cout << "Scavtrap default constructor called" << std::endl;
	return ;
}

ScavTrap::ScavTrap( ScavTrap const & s ) {

	std::cout << "Scavtrap copy constructor called" << std::endl;
	*this = s;
	return ;
}

ScavTrap::~ScavTrap( void ) {

	std::cout << "ScavTrap destructor called" << std::endl;
	return ;
}

//---------------------- OPERATORS ------------------------------------------------

ScavTrap & ScavTrap::operator=( ScavTrap const & s ) {

	std::cout << "ScavTrap assignation operator called" << std::endl;
	if (this != &s)
		ClapTrap::operator=(s);
	return (*this);
}

//------------------------- FUNCTIONS --------------------------------------------

void ScavTrap::attack( std::string const & target ) {

	if (!this->checkEnergy() || !this->checkHit())
		return ;
	std::cout << "ScavTrap " << this->getName()
			  << " attacks " << target << ", causing "
			  << this->getAttack() << " points of damage";
	if (target == this->getName())
		std::cout << " (" << this->getName() << " is a bit of a masochist)";
	std::cout << "." << std::endl;
	this->_energy--;
}

void ScavTrap::guardGate( void ) {

	if (!this->checkHit() || !this->checkEnergy())
		return ;
	std::cout << "ScavTrap " << this->getName() << " is now in gate keeper mode" << std::endl;
	return ;
}

//------------------------ SETTERS -------------------------------------

void ScavTrap::setEnergy( void ) {

	this->_energy = 50;
	return ;
}