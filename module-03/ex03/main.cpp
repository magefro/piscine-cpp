//
// Created by alangloi on 1/27/22.
//

#include "ClapTrap.hpp"
#include "ScavTrap.hpp"
#include "FragTrap.hpp"
#include "DiamondTrap.hpp"

#define PRINT print_status(bidule); print_status(machin);


void print_status(ClapTrap const & who) {

	std::cout << "++++++++INFOS++++++" << std::endl;
	std::cout << "name:   " << who.getName() << std::endl;
	std::cout << "hit:    " << who.getHit() << std::endl;
	std::cout << "energy: " << who.getEnergy() << std::endl;
    std::cout << "attack: " << who.getAttack() << std::endl;
	std::cout << "+++++++++++++++++++" << std::endl;
}

int main() {

	ClapTrap *thing;
	DiamondTrap bidule("bidule");
	DiamondTrap machin("machin");
	DiamondTrap monster;
	ScavTrap truc("truc");
	FragTrap pouet("pouet");

	thing = &monster;
	std::cout << monster.getHit() << std::endl;
	std::cout << "-------------- ClapTrap functions test --------" << std::endl;
	machin.takeDamage(2);
	machin.beRepaired(2);
	bidule.takeDamage(3);
	thing->takeDamage(4);
	std::cout << "-------------- Attributes test --------" << std::endl;
	machin.attack(bidule.getName());
	truc.attack(bidule.getName());
	bidule.attack(truc.getName());
	bidule.attack(machin.getName());
	thing->attack(machin.getName());
	std::cout << "-------------- Special functions test --------" << std::endl;
	truc.guardGate();
	pouet.highFivesGuys();
	std::cout << "------------- Personality test ---------------" << std::endl;
	bidule.whoAmI();
	machin.whoAmI();
	std::cout << "----------------------------------------------" << std::endl;
	return (0);
}