//
// Created by alangloi on 1/27/22.
//


#ifndef PISCINE_CPP_CLAPTRAP_HPP
#define PISCINE_CPP_CLAPTRAP_HPP


#include <iostream>

class ClapTrap {

protected:
	std::string _name;
	int _hit;
	int _energy;
	int _attack;

public:
	ClapTrap(const std::string &name);
	ClapTrap(const std::string &name, int hit, int energy, int attack);
	ClapTrap( void );
	virtual ~ClapTrap( void );
	ClapTrap ( ClapTrap const & c);

	ClapTrap &operator=( ClapTrap const & c );

	virtual void	attack(const std::string & target);
	void	takeDamage(unsigned int amount);
	void	beRepaired(unsigned int amount);
	bool	checkHit( void );
	bool	checkEnergy( void );

	std::string getName( void ) const;
	int 	getHit( void ) const;
	int		getEnergy( void ) const;
	int 	getAttack( void ) const;
};

//std::ostream & operator<<(std::ostream & o, ClapTrap const & rhs);

#endif //PISCINE_CPP_CLAPTRAP_HPP