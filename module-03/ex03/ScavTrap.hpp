//
// Created by alangloi on 1/27/22.
//

#ifndef PISCINE_CPP_SCAVTRAP_HPP
#define PISCINE_CPP_SCAVTRAP_HPP

#include <iostream>
#include "ClapTrap.hpp"

class ScavTrap : virtual public ClapTrap {

public:
	ScavTrap( std::string name);
	ScavTrap( void );
	ScavTrap( ScavTrap const & s );
	virtual ~ScavTrap( void );

	ScavTrap & operator=( ScavTrap const & s );

	void setEnergy( void );

	void guardGate( void );
	void attack( std::string const & target );

};


#endif //PISCINE_CPP_SCAVTRAP_HPP
