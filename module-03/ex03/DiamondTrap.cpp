//
// Created by alangloi on 1/27/22.
//

#include "DiamondTrap.hpp"
#include "ClapTrap.hpp"
#include "ScavTrap.hpp"
#include "FragTrap.hpp"

const std::string diamondName = "Default_DiamondTrap";
const std::string clapName = "_clap_name";

//------------------- CONSTRUCTORS -----------------------

DiamondTrap::DiamondTrap( std::string name ) : ClapTrap(name + clapName), ScavTrap(), FragTrap(), _name(name) {

	std::cout << "DiamondTrap constructor called" << std::endl;
	this->FragTrap::setHit();
	this->ScavTrap::setEnergy();
	this->FragTrap::setAttack();
	return ;
}

DiamondTrap::DiamondTrap( DiamondTrap const & d) {

	std::cout << "DiamondTrap copy constructor called" << std::endl;
	*this = d;
	return ;
}

DiamondTrap::DiamondTrap( void ) : ClapTrap( diamondName + clapName), ScavTrap(), FragTrap(), _name(diamondName) {

	std::cout << "DiamondTrap default constructor called" << std::endl;
	this->FragTrap::setHit();
	this->ScavTrap::setEnergy();
	this->FragTrap::setAttack();
	return ;
}

DiamondTrap::~DiamondTrap( void ) {

	std::cout << "DiamondTrap destructor called" << std::endl;
	return ;
}

//-------------------------- OPERATORS ----------------------------

DiamondTrap &DiamondTrap::operator=( DiamondTrap const & d ) {

	std::cout << "DiamondTrap assignation operator called" << std::endl;
	if (this != &d)
		ClapTrap::operator=(d);
	return (*this);
}

//-------------------------------- ACTIONS -------------------------------

void DiamondTrap::whoAmI( void ) {

	if (!this->checkHit() || !this->checkEnergy())
		return ;
	std::cout << "My DiamondTrap name is " << this->_name
			  << " and my ClapTrap name is " << this->getName()
			  << std::endl;
	return ;
}