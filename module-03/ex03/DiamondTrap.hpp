//
// Created by alangloi on 1/27/22.
//

#ifndef PISCINE_CPP_DIAMONDTRAP_HPP
#define PISCINE_CPP_DIAMONDTRAP_HPP

#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "ClapTrap.hpp"

class DiamondTrap : public ScavTrap, public FragTrap {

private:
	std::string _name;

public:
	DiamondTrap( std::string name );
	DiamondTrap( DiamondTrap const & d);
	DiamondTrap( void );
	virtual ~DiamondTrap( void );

	DiamondTrap &operator=( DiamondTrap const & d );

	void whoAmI( void );
};


#endif //PISCINE_CPP_DIAMONDTRAP_HPP
