//
// Created by alangloi on 1/27/22.
//

#ifndef PISCINE_CPP_FRAGTRAP_HPP
#define PISCINE_CPP_FRAGTRAP_HPP

#include "ClapTrap.hpp"

class FragTrap : virtual public ClapTrap {

public:
	FragTrap( std::string const & name);
	FragTrap( FragTrap const & f );
	FragTrap( void );
	virtual ~FragTrap( void );

	FragTrap &operator=( FragTrap const & f );

	void	setHit( void );
	void	setAttack( void );

	void	highFivesGuys( void );
	void 	attack( std::string const & target );

};


#endif //PISCINE_CPP_FRAGTRAP_HPP
