//
// Created by alangloi on 1/27/22.
//

#include "FragTrap.hpp"

//----------------------------- CONSTRUCTORS --------------------------------------

FragTrap::FragTrap( std::string const & name) : ClapTrap(name, 100, 100, 30) {

	std::cout << "FragTrap constructor called" << std::endl;
	return ;
}

FragTrap::FragTrap( FragTrap const & f ) {

	std::cout << "FragTrap copy constructor called" << std::endl;
	*this = f;
	return ;
}

FragTrap::FragTrap( void ) : ClapTrap("Default", 100, 100, 30) {

	std::cout << "FragTrap default constructor called" << std::endl;
	return ;
}

FragTrap::~FragTrap( void ) {

	std::cout << "FragTrap destructor called" << std::endl;
	return ;
}

//-------------------------------- OPERATORS ----------------------------------------

FragTrap &FragTrap::operator=( FragTrap const & f ) {

	std::cout << "FragTrap assignation operator called" << std::endl;
	if (this != &f)
		ClapTrap::operator=(f);
	return (*this);
}

//--------------------------------- ACTIONS ----------------------------------------

void FragTrap::attack( std::string const & target ) {

	if (!this->checkEnergy() || !this->checkHit())
		return ;
	std::cout << "FragTrap " << this->getName()
			  << " attacks " << target << ", causing "
			  << this->getAttack() << " points of damage";
	if (target == this->getName())
		std::cout << " (" << this->getName() << " is a bit of a masochist)";
	std::cout << "." << std::endl;
	this->_energy--;
}

void	FragTrap::highFivesGuys( void ) {

	if (!this->checkHit() || !this->checkEnergy())
		return ;
	std::cout << "High Five request got accepted by " << this->getName() << std::endl;
	return ;
}