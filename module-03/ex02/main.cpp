//
// Created by alangloi on 1/27/22.
//

#include "ClapTrap.hpp"
#include "ScavTrap.hpp"
#include "FragTrap.hpp"
#define PRINT print_status(bidule); print_status(machin);

void print_status(ClapTrap const & who) {

	std::cout << "++++++++INFOS++++++" << std::endl;
	std::cout << "name:   " << who.getName() << std::endl;
	std::cout << "hit:    " << who.getHit() << std::endl;
	std::cout << "energy: " << who.getEnergy() << std::endl;
	std::cout << "attack: " << who.getAttack() << std::endl;
	std::cout << "+++++++++++++++++++" << std::endl;
}

int main() {

	ClapTrap truc("truc");
	ScavTrap pouet("pouet");
	FragTrap bidule("bidule");
	FragTrap machin("machin");

	print_status(truc);
	print_status(pouet);
	truc.attack(machin.getName());
	bidule.attack(machin.getName());
	pouet.attack(bidule.getName());
	bidule.highFivesGuys();
	bidule.highFivesGuys();
	machin.takeDamage(20);
	machin.beRepaired(20);
	PRINT;
	bidule.takeDamage(100);
	bidule.attack(machin.getName());
	bidule.highFivesGuys();
	PRINT;
	return (0);
}