//
// Created by alangloi on 1/27/22.
// Checker pourquoi constructor par copie et assignation operator appele quand getters

#include "ClapTrap.hpp"
#include "ScavTrap.hpp"
#define PRINT print_status(bidule); print_status(machin); print_status(truc);

void print_status(ClapTrap const & who) {

	std::cout << "++++++++INFOS++++++" << std::endl;
	std::cout << "name:   " << who.getName() << std::endl;
	std::cout << "hit:    " << who.getHit() << std::endl;
	std::cout << "energy: " << who.getEnergy() << std::endl;
	std::cout << "attack: " << who.getAttack() << std::endl;
	std::cout << "+++++++++++++++++++" << std::endl;
}

int main() {

	ClapTrap truc("truc");
	ScavTrap bidule("bidule");
	ScavTrap machin("machin");

	truc.attack(bidule.getName());
	bidule.attack(machin.getName());
	machin.takeDamage(20);
	machin.beRepaired(20);
	PRINT;
	machin.attack(bidule.getName());
	bidule.takeDamage(10);
	bidule.attack(machin.getName());
	bidule.guardGate();
	PRINT;
	return (0);
}