//
// Created by alangloi on 1/27/22.
//

#ifndef PISCINE_CPP_SCAVTRAP_HPP
#define PISCINE_CPP_SCAVTRAP_HPP


#include <iostream>
#include "ClapTrap.hpp"

class ScavTrap : public ClapTrap {

public:
	ScavTrap( std::string name);
	ScavTrap( void );
	ScavTrap( ScavTrap const & s );
	~ScavTrap( void );

	ScavTrap & operator=( ScavTrap const & s );

	void attack( std::string const & target );

	void guardGate( void );

};


#endif //PISCINE_CPP_SCAVTRAP_HPP
