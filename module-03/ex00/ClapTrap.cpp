//
// Created by alangloi on 1/27/22.
//

#include "ClapTrap.hpp"

//---------------------------------- CONSTRUCTORS -------------------------------------------

ClapTrap::ClapTrap(const std::string &name) : _name(name), _hit(10), _energy(10), _attack(0) {

	std::cout << "Constructor called" << std::endl;
	return ;
}

ClapTrap::ClapTrap( ClapTrap const & c) {

	std::cout << "Copy constructor called" << std::endl;
	*this = c;
	return ;
}

ClapTrap::ClapTrap() : _name("Default"), _hit(10), _energy(10), _attack(0) {

	std::cout << "Default constructor called" << std::endl;
	return ;
}

ClapTrap::~ClapTrap() {

	std::cout << "Destructor called" << std::endl;
	return ;
}


//----------------------------------- ACTIONS ----------------------------------------------

void	ClapTrap::attack(const std::string & target) {

	if (!this->checkEnergy() || !this->checkHit())
		return ;
	std::cout << "ClapTrap " << this->_name
			  << " attacks " << target << ", causing "
			  << this->_attack << " points of damage";
	if (target == this->_name)
		std::cout << " (" << this->_name << " is a bit of a masochist)";
			  std::cout << "." << std::endl;
	this->_energy--;
}

void	ClapTrap::takeDamage(unsigned int amount) {

	if (!this->checkHit())
		return ;
	std::cout << "ClapTrap " << this->_name
			  << " takes " << amount << " damages."
			  << std::endl;
	this->_hit -= amount;
}

void	ClapTrap::beRepaired(unsigned int amount) {

	if (!this->checkEnergy() || !this->checkHit())
		return ;
	std::cout << "ClapTrap " << this->_name
			  << " be repaired of " << amount << " points."
			  << std::endl;
	this->_hit += amount;
	this->_energy--;
}

//------------------------------- GETTERS ---------------------------------

std::string ClapTrap::getName( void ) const {

	return (this->_name);
}

int 	ClapTrap::getHit( void ) const {

	return (this->_hit);
}

int		ClapTrap::getEnergy( void ) const {

	return (this->_energy);
}

int		ClapTrap::getAttack( void ) const {

	return (this->_attack);
}

//-------------------------- CHECKERS --------------------------

bool	ClapTrap::checkHit( void ) {

	if (this->getHit() <= 0)
	{
		this->_hit = 0;
		std::cout << "ClapTrap " << this->getName() << " is dead." << std::endl;
		return (false);
	}
	return (true);
}

bool	ClapTrap::checkEnergy( void ) {

	if (this->getEnergy() <= 0)
	{
		std::cout << "ClapTrap " << this->getName() << " is too tired." << std::endl;
		return (false);
	}
	return (true);
}

//---------------------- OPERATORS -----------------------------------------

ClapTrap &ClapTrap::operator=( ClapTrap const & c ) {

	std::cout << "ClapTrap assignation operator called" << std::endl;
	if (this != &c)
	{
		this->_name = c.getName();
		this->_hit = c.getHit();
		this->_attack = c.getAttack();
		this->_energy = c.getEnergy();
	}
	return (*this);
}