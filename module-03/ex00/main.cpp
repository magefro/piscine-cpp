//
// Created by alangloi on 1/27/22.
//

#include "ClapTrap.hpp"
#define PRINT print_status(bidule); print_status(machin);

void print_status(ClapTrap const & who) {

	std::cout << "++++++++INFOS++++++" << std::endl;
	std::cout << "name:   " << who.getName() << std::endl;
	std::cout << "hit:    " << who.getHit() << std::endl;
	std::cout << "energy: " << who.getEnergy() << std::endl;
	std::cout << "attack: " << who.getAttack() << std::endl;
	std::cout << "+++++++++++++++++++" << std::endl;
}

int main() {

	ClapTrap bidule("bidule");
	ClapTrap machin("machin");

	bidule.attack(machin.getName());
	machin.takeDamage(2);
	machin.beRepaired(2);
	PRINT;
	machin.attack(bidule.getName());
	bidule.takeDamage(11);
	bidule.attack(machin.getName());
	bidule.attack(machin.getName());
	PRINT;
	machin.attack(bidule.getName());
	machin.attack(bidule.getName());
	machin.attack(bidule.getName());
	machin.attack(bidule.getName());
	machin.attack(bidule.getName());
	machin.attack(bidule.getName());
	machin.attack(bidule.getName());
	machin.attack(bidule.getName());
	PRINT;
	machin.attack(bidule.getName());
	machin.attack(bidule.getName());
	PRINT;

}