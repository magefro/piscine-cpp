//
// Created by alangloi on 1/27/22.
//


#ifndef PISCINE_CPP_CLAPTRAP_HPP
#define PISCINE_CPP_CLAPTRAP_HPP


#include <iostream>

class ClapTrap {

private:
	std::string _name;
	int _hit;
	int _energy;
	int _attack;

public:
	ClapTrap(const std::string &name);
	ClapTrap( ClapTrap const & c);
	ClapTrap();
	~ClapTrap();

	ClapTrap &operator=( ClapTrap const & c );

	void	attack(const std::string & target);
	void	takeDamage(unsigned int amount);
	void	beRepaired(unsigned int amount);

	bool	checkHit( void );
	bool	checkEnergy( void );

	std::string getName( void ) const;
	int 	getHit( void ) const;
	int		getEnergy( void ) const;
	int 	getAttack( void ) const;

};


#endif //PISCINE_CPP_CLAPTRAP_HPP
