//
// Created by antoine on 25/02/2022.
//

#include "Base.hpp"
#include <cstdlib>

//------------- DESTRUCTOR ---------------

Base::~Base() {

	std::cout << "Base destructor called" << std::endl;
	return ;
}

//------------- FUNCIONS ----------------

Base	*generate( void ) {

	int nb;
	nb = std::rand() % 3;

	switch (nb) {
		case 0:
			std::cout << "A";
			return (new A);
		case 1:
			std::cout << "B";
			return (new B);
		case 2:
			std::cout << "C";
			return (new C);
	}
	return (NULL);
}

void	identify( Base * p ) {

		A *ptr_a = dynamic_cast<A *>(p);
		if (ptr_a != NULL) {
			std::cout << "A";
			(void) ptr_a;
			return;
		}
		B *ptr_b = dynamic_cast<B *>(p);
		if (ptr_b != NULL) {
			std::cout << "B";
			(void) ptr_b;
			return;
		}
		C *ptr_c = dynamic_cast<C *>(p);
		if (ptr_c != NULL) {
			std::cout << "C";
			(void) ptr_c;
			return;
		}
}

void	identify( Base & p ) {

	try {
		A &ref_a = dynamic_cast<A &>(p);
		(void)ref_a;
		std::cout << "A";
	} catch (std::bad_cast& e) {}
	try {
		B &ref_b = dynamic_cast<B &>(p);
		(void)ref_b;
		std::cout << "B";
	} catch (std::bad_cast& e) {}
	try {
		C &ref_c = dynamic_cast<C &>(p);
		(void)ref_c;
		std::cout << "C";
	} catch (std::bad_cast& e) {}

}