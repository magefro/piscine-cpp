//
// Created by antoine on 25/02/2022.
//

#ifndef PISCINE_CPP_BASE_HPP
#define PISCINE_CPP_BASE_HPP

#include <iostream>

class Base {

public:
	virtual ~Base();

};

class A : public Base {};
class B : public Base {};
class C : public Base {};

Base	*generate( void );
void	identify( Base * p );
void	identify( Base & p );

#endif //PISCINE_CPP_BASE_HPP
