//
// Created by antoine on 25/02/2022.
//

#include "Base.hpp"

int main() {

	for (int i = 0; i < 10; i++)
	{
		std::cout << "generating base " << i << ": ";
		Base *b = generate();
		std::cout << std::endl << "identifying (pointer): ";
		identify(b);
		std::cout << std::endl << "identifying (reference): ";
		identify(*b);
		std::cout << std::endl;
		delete b;
		std::cout << std::endl;
	}
	Base *a = NULL;
	identify(a);
	identify(*a);
	return (0);
}