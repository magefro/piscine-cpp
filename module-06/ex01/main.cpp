//
// Created by antoine on 25/02/2022.
//

#include "Data.hpp"
#include <stdint.h>

int main() {

	Data data;
	Data *data_ptr = &data;
	uintptr_t uint_ptr;

	data_ptr->content = "Hello World!";

	std::cout << std::endl << "data_ptr: " << data_ptr << std::endl;
	std::cout << "content: " << data_ptr->content << std::endl;

	std::cout << std::endl << "* serializing data_ptr *" << std::endl;
	uint_ptr = data.serialize(data_ptr);

	data_ptr = NULL;
	std::cout << std::endl << "data_ptr (set to NULL): " << data_ptr << std::endl;
	std::cout << "uint_ptr: " << uint_ptr << std::endl;

	std::cout << std::endl << "* deserializing data_ptr *" << std::endl;
	data_ptr = data.deserialize(uint_ptr);

	std::cout << std::endl << "data_ptr: " << data_ptr << std::endl;
	std::cout << "content: " << data_ptr->content << std::endl;

	std::cout << std::endl;

	return (0);
}