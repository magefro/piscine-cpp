//
// Created by antoine on 25/02/2022.
//

#include "Data.hpp"

//------------------------ CONSTRUCTORS ----------------------

Data::Data() {

	std::cout << "Data default constructor called" << std::endl;
	return ;
}

Data::~Data() {

	std::cout << "Data destructor called" << std::endl;
	return ;
}

Data::Data( Data const & d ) {

	std::cout << "Data copy constructor called" << std::endl;
	*this = d;
	return ;
}

//------------------------ OPERATORS -----------------------

Data &Data::operator=( Data const & d ) {

	std::cout << "Data assignation operator called" << std::endl;
	if (this != &d)
	{
		this->content = d.content;
	}
	return (*this);
}

//------------------------ FUNCTIONS ----------------------

uintptr_t	Data::serialize( Data * ptr ) {

	return (reinterpret_cast<uintptr_t>(ptr));
}

Data *		Data::deserialize( uintptr_t raw ) {

	return (reinterpret_cast<Data *>(raw));
}