//
// Created by antoine on 25/02/2022.
//

#ifndef PISCINE_CPP_DATA_HPP
#define PISCINE_CPP_DATA_HPP

#include <stdint.h>
#include <iostream>

class Data {

public:
	Data();
	~Data();
	Data( Data const & d );
	Data &operator=( Data const & d );

	uintptr_t	serialize( Data * ptr );
	Data *		deserialize( uintptr_t raw );

	std::string content;
};


#endif //PISCINE_CPP_DATA_HPP
