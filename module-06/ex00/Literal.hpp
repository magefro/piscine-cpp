//
// Created by alangloi on 2/22/22.
//

#ifndef PISCINE_CPP_LITERAL_HPP
#define PISCINE_CPP_LITERAL_HPP

#include <iostream>

class Literal {
private:
	std::string _s_lit;
	double		_d_lit;
	std::string _type;
	Literal();

public:
	~Literal();
	Literal( Literal const & l );
	Literal( std::string lit );

	Literal &operator=( Literal const & l );

	std::string	getLiteralString( void ) const;
	double		getLiteralDouble( void ) const;

	char		getChar( void ) const;
	int 		getInteger( void ) const;
	float		getFloat( void ) const;
	double		getDouble( void ) const;
	std::string getType( void ) const;

	void		checkType();
	bool		checkNAN() const;

	class 		nonDisplayableException: public std::exception {
	public:
		virtual const char * what() const throw() {
			return ("non displayable");
		}
	};

	class 		castErrorException: public std::exception {
	public:
		virtual const char * what() const throw() {
			return ("impossible");
		}
	};

};


#endif //PISCINE_CPP_LITERAL_HPP
