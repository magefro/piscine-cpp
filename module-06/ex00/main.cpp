//
// Created by Antoine LANGLOIS on 2/18/22.
//

#include <iostream>
#include "Literal.hpp"
#include <cmath>
#include <iomanip>

int main (int argc, char **argv)
{
	std::string str = static_cast<std::string>(argv[1]);

	if (argc != 2 || str.length() == 0)
	{
		std::cout << "Wrong number of arguments. Type ./convert <char, int, float, double literal>" << std::endl;
		return (1);
	}
	Literal literal(str);

	std::cout << "char: ";
	try {
		char c = literal.getChar();
		std::cout << "\'" << c << "\'" << std::endl;
	} catch ( std::exception& e ) {
		std::cout << e.what() << std::endl;
	}

	std::cout << "int: ";
	try {
		int i = literal.getInteger();
		std::cout << i << std::endl;
	} catch ( std::exception& e ) {
		std::cout << e.what() << std::endl;
	}

	double roundpart = 0;

	std::cout << "float: ";
	try {
		float f = literal.getFloat();
		std::cout << f;
		if (!(modf(f,&roundpart)) && !isinf(f))
			std::cout << ".0";
		std::cout << 'f' << std::endl;
	} catch ( std::exception& e ) {
		std::cout << e.what() << std::endl;
	}

	roundpart = 0;

	std::cout << "double: ";
	try {
		double d = literal.getDouble();
		std::cout << std::setprecision(17) << d;
		if (!(modf(d, &roundpart)) && !isinf(d))
			std::cout << ".0";
		std::cout << std::endl;
	} catch ( std::exception& e ) {
		std::cout << e.what() << std::endl;
	}
	return (0);
}