//
// Created by alangloi on 2/22/22.
//

#include "Literal.hpp"
#include <string>
#include <cstdlib>
#include <limits>
#include <cmath>

std::string notANumber[12] = {
		"inf",
		"inff",
		"-inf",
		"-inff",
		"+inf",
		"+inff",
		"nan",
		"-nan",
		"+nan",
		"nanf",
		"-nanf",
		"+nanf",
};

//------------------------- CONSTRUCTORS ------------------

Literal::Literal( std::string lit ) : _s_lit(lit) {

	//std::cout << "Literal constructor called" << std::endl;
	try {
		this->_d_lit = static_cast<double>(std::strtod(lit.c_str(), NULL));
	} catch ( std::exception& e ) {
		throw Literal::castErrorException();
	}
	this->checkType();
	return ;
}

Literal::Literal() {

	//std::cout << "Literal default constructor called" << std::endl;
	return ;
}

Literal::~Literal() {

	//std::cout << "Literal destructor called" << std::endl;
	return ;
}

Literal::Literal( Literal const & l ) {

	//std::cout << "Literal copy constructor called" << std::endl;
	if (this != &l)
	{
		this->_s_lit = l._s_lit;
		this->_d_lit = l._d_lit;
	}
	return ;
}

//------------------------- OPERATORS ---------------------

Literal &Literal::operator=( Literal const & l ) {

	//std::cout << "Literal assignation operator called" << std::cout;
	this->_s_lit = l.getLiteralString();
	this->_d_lit = l.getLiteralDouble();
	return (*this);
}

//-------------------------- CHECKERS ---------------------

void Literal::checkType() {

	int i = 0;

	if (this->_s_lit.length() == 1 && std::isprint(this->_s_lit.c_str()[0]) && !std::isdigit(this->_s_lit.c_str()[0]))
	{
		this->_type = "char";
		return ;
	}
	if ((this->_s_lit.c_str()[0] == '-' || this->_s_lit.c_str()[0] == '+') && this->_s_lit.length() > 1  && std::isdigit(this->_s_lit.c_str()[1]))
		i++;
	while (std::isdigit(this->_s_lit.c_str()[i]))
		i++;
	if (this->_s_lit.c_str()[i] == '.')
	{
		i++;
		while (std::isdigit(this->_s_lit.c_str()[i]))
			i++;
		if (i == static_cast<int>(this->_s_lit.length()))
		{
			this->_type = "double";
			return ;
		}
		if (this->_s_lit.c_str()[i] == 'f')
		{
			this->_type = "float";
			return ;
		}
		else
		{
			this->_type = "non displayable";
			return ;
		}
	}
	if (i == static_cast<int>(this->_s_lit.length()))
	{
		this->_type = "integer";
		return ;
	}
	else
	{
		this->_type = "non displayable";
		return ;
	}
}

bool	Literal::checkNAN() const {

	for (int i = 0; i < 12; i++)
	{
		if (notANumber[i] == this->_s_lit)
			return (true);
	}
	return (false);
}

//-------------------------- GETTERS ----------------------

std::string Literal::getLiteralString( void ) const {

	return (this->_s_lit);
}

double Literal::getLiteralDouble( void ) const {

	return (this->_d_lit);
}

double	Literal::getDouble( void ) const {

	if ( this->_s_lit.length() == 1 && this->_type == "char")
		return (static_cast<double>(this->_s_lit.c_str()[0]));
	return (this->_d_lit);
}

float 	Literal::getFloat( void ) const {

	if ( this->_s_lit.length() == 1 && this->_type == "char")
		return (static_cast<float>(this->_s_lit.c_str()[0]));
	return (static_cast<float>(this->_d_lit));
}

int 	Literal::getInteger( void ) const {

	if (checkNAN())
		throw Literal::castErrorException();
	if (this->_type == "char" && this->_s_lit.length() == 1)
		return (static_cast<int>(this->_s_lit[0]));
	try {
		if ( this->_d_lit < std::numeric_limits<int>::min() || this->_d_lit > std::numeric_limits<int>::max() )
			throw Literal::castErrorException();
		return (static_cast<int>(this->_d_lit));
	} catch ( std::exception & e ) {
		throw Literal::castErrorException();
	}
}

char	Literal::getChar( void ) const {

	if (this->_type == "char")
	{
		if (this->_s_lit.length() > 1)
			throw Literal::castErrorException();
		if (std::isprint(this->_s_lit.c_str()[0]) && !std::isdigit(this->_s_lit.c_str()[0]))
			return (this->_s_lit[0]);
	}
	try
	{
		if (checkNAN())
			throw Literal::castErrorException();
		if (static_cast<int>(this->_d_lit) < 32 || static_cast<int>(this->_d_lit) > 126)
			throw Literal::nonDisplayableException();
		return (static_cast<char>(this->_d_lit));
	} catch ( std::logic_error & e ) {
		throw Literal::castErrorException();
	}
}

std::string Literal::getType( void ) const {

	return (this->_type);
}