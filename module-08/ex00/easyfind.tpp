//
// Created by Antoine LANGLOIS on 2/28/22.
//

#ifndef PISCINE_CPP_EASYFIND_TPP
#define PISCINE_CPP_EASYFIND_TPP

#include <iostream>
#include <algorithm>

template< typename T >
typename T::const_iterator	easyfind( T const & c, int const & find ) {

	typename T::const_iterator it = std::find( c.begin(), c.end(), find );

	if (it == c.end())
		throw std::out_of_range("can't find element");
	return (it);
}

#endif //PISCINE_CPP_EASYFIND_TPP
