//
// Created by Antoine LANGLOIS on 2/28/22.
//

#include "easyfind.tpp"
#include <vector>
#include <list>

/*
template< typename T >
std::ostream &operator<<( std::ostream & o, typename T::const_iterator & vec ) {

	o << vec;
	return (o);
}
*/

int main() {

	std::vector<int>	v;
	std::list<int>		l;

	v.push_back(3);
	v.push_back(6);
	v.push_back(9);

	l.push_back(2);
	l.push_back(4);
	l.push_back(6);

	std::cout << "trying to find iterator in previous containers:" << std::endl;
	try {
		std::vector<int>::const_iterator v_it = easyfind(v, 3);
		std::list<int>::const_iterator l_it = easyfind(l, 4);
		std::cout << *v_it << std::endl;
		std::cout << *l_it << std::endl;
	} catch ( std::exception & e ) {
		std::cout << e.what() << std::endl;
	}

	std::cout << std::endl << "trying to find iterator in previous containers:" << std::endl;
	try {
		std::vector<int>::const_iterator v_it = easyfind(v, 10);
		std::list<int>::const_iterator l_it = easyfind(l, 67);
		std::cout << *v_it << std::endl;
		std::cout << *l_it << std::endl;
	} catch ( std::exception & e ) {
		std::cout << e.what() << std::endl;
	}

	std::cout << std::endl << "trying to find iterator in empty containers:" << std::endl;
	std::vector<int> empty_vec;
	try {
		std::vector<int>::const_iterator e_it = easyfind(empty_vec, 10);
		std::cout << *e_it << std::endl;
	} catch ( std::exception & e ) {
		std::cout << e.what() << std::endl;
	}

	return (0);
}