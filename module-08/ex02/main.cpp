//
// Created by Antoine LANGLOIS on 2/28/22.
//

#include "MutantStack.tpp"

int main() {

	std::cout << std::endl << "creating mutant stack and pushing values to it:" << std::endl;
	MutantStack<int> mstack;
	mstack.push(5);
	mstack.push(17);

	std::cout << std::endl << "getting element of the top list:" << std::endl;
	std::cout << mstack.top() << std::endl;

	std::cout << std::endl << "removing element of the top list:" << std::endl;
	mstack.pop();

	std::cout << std::endl << "getting stack size:" << std::endl;
	std::cout << mstack.size() << std::endl;

	std::cout << std::endl << "adding more elements:" << std::endl;
	mstack.push(3);
	mstack.push(5);
	mstack.push(737);
	//[...]
	mstack.push(0);

	std::cout << std::endl << "creating two iterator and printing the whole stack:" << std::endl;
	MutantStack<int>::iterator it = mstack.begin();
	MutantStack<int>::iterator ite = mstack.end();
	++it;
	--it;
	while (it != ite)
	{
		std::cout << *it << std::endl;
		++it;
	}

	std::cout << std::endl << "creating new stack by copy:" << std::endl;
	std::stack<int> s(mstack);

	std::cout << std::endl << std::endl << "----- doing the same with list: -----" << std::endl;

	std::cout << std::endl << "creating mutant stack and pushing values to it:" << std::endl;
	std::list<int> lst;
	lst.push_back(5);
	lst.push_back(17);

	std::cout << std::endl << "getting element of the back list:" << std::endl;
	std::cout << lst.back() << std::endl;

	std::cout << std::endl << "removing element of the back list:" << std::endl;
	lst.pop_back();

	std::cout << std::endl << "getting stack size:" << std::endl;
	std::cout << lst.size() << std::endl;

	std::cout << std::endl << "adding more elements:" << std::endl;
	lst.push_back(3);
	lst.push_back(5);
	lst.push_back(737);
	//[...]
	lst.push_back(0);

	std::cout << std::endl << "creating two iterator and printing the whole stack:" << std::endl;
	std::list<int>::iterator itl = lst.begin();
	std::list<int>::iterator itel = lst.end();
	++itl;
	--itl;
	while (itl != itel)
	{
		std::cout << *itl << std::endl;
		++itl;
	}

	return (0);
}