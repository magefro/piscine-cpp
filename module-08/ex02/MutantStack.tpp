//
// Created by Antoine LANGLOIS on 2/28/22.
//

#ifndef PISCINE_CPP_MUTANTSTACK_TPP
#define PISCINE_CPP_MUTANTSTACK_TPP

#include <iostream>
#include <stack>
#include <list>
#include <vector>

template< typename T>
class MutantStack : public std::stack<T> {
public:
	MutantStack() {};
	~MutantStack() {};
	MutantStack( MutantStack<T> const & ms ) {
		this->c = ms.c; };
	MutantStack &operator=( MutantStack<T> const & ms ) {
		if (this != &ms) {
			this->c = ms.c; }
		return (*this); };

	typedef typename std::stack<T>::container_type::iterator iterator;
	iterator begin() {
		return (this->c.begin()); };
	iterator end() {
		return (this->c.end()); };
};

#endif //PISCINE_CPP_MUTANTSTACK_TPP
