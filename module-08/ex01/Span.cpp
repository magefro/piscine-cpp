//
// Created by Antoine LANGLOIS on 2/28/22.
//

#include "Span.hpp"

//----------------------- CONSTRUCTORS --------------------

Span::Span( void ) : _size(0) {

	std::cout << "Span default constructor called" << std::endl;
	return ;
}

Span::~Span( void ) {

	std::cout << "Span destructor called" << std::endl;
	return ;
}

Span::Span( Span const & s ) {

	std::cout << "Span copy constructor called" << std::endl;
	this->_size = s._size;
	this->_elem = s._elem;
	return ;
}

Span::Span( unsigned int const & N ) : _size(N) {

	std::cout << "Span constructor called" << std::endl;

	return ;
}

//----------------------- OPERATORS -----------------------

Span 				&Span::operator=( Span const & s ) {

	std::cout << "Span assignation operator called" << std::endl;
	if (this != &s)
	{
		this->_size = s._size;
		this->_elem = s._elem;
	}
	return (*this);
}

//---------------------- GETTERS -------------------------

unsigned int 		Span::getSize() const {

	return (this->_size);
}

unsigned int 		Span::getElem( unsigned int const & nb ) const {

	return (this->_elem[nb]);
}

//----------------------- FUNCTIONS -----------------------

void 			Span::addNumber( int const & nb ) {

	if (this->_elem.size() < this->_size)
		this->_elem.push_back(nb);
	else
		throw Span::FullSpanException();
	return ;
}

void 			Span::addNumber( std::vector<int>::iterator a, std::vector<int>::iterator b ) {

	if (distance(a, b) > this->getSize())
		throw Span::OutOfRangeException();
	_elem.insert( _elem.begin(), a, b);
}

unsigned int	Span::shortestSpan( void ) const {

	if (this->_elem.size() < 2)
		throw Span::EmptySpanException();

	std::multiset<int> cont(this->_elem.begin(), this->_elem.end());
	std::multiset<int>::iterator it = cont.begin();
	unsigned int s, t, u;

	s = UINT_MAX;
	u = *it;
	it++;
	for (; it != cont.end(); ++it)
	{
		t = *it - u;
		if (t < s)
			s = t;
		u = *it;
	}
	return (s);
}

unsigned int	Span::longestSpan( void ) const {

	if (this->_elem.size() < 2)
		throw Span::EmptySpanException();

	std::multiset<int> cont(this->_elem.begin(), this->_elem.end());
	unsigned int result = *(cont.rbegin()) - *(cont.begin());

	return (result);
}
