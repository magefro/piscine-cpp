//
// Created by Antoine LANGLOIS on 2/28/22.
//

#include "Span.hpp"

int main() {

	std::cout << std::endl << "creating span <= 1 and searching longest and shortest:" << std::endl;
	Span em = Span(0);
	Span pty = Span(1);

	try {
		em.longestSpan();
	} catch ( std::exception &e ) {
		std::cout << e.what() << std::endl;
	}

	try {
		pty.shortestSpan();
	} catch ( std::exception &e ) {
		std::cout << e.what() << std::endl;
	}

	std::cout << std::endl << "creating a small span and trying to add different numbers:" << std::endl;
	Span sp = Span(5);

	try {
		sp.addNumber(6);
		sp.addNumber(3);
		sp.addNumber(17);
		sp.addNumber(9);
		sp.addNumber(10);
	} catch (std::exception & e) {
		std::cout << e.what() << std::endl;
	}

	std::cout << "longest span: " << sp.longestSpan() << std::endl;
	std::cout << "shortest span: " << sp.shortestSpan() << std::endl;

	std::cout << std::endl << "adding one more number (ooops):" << std::endl;
	try {
		sp.addNumber(94503);
	} catch (std::exception & e) {
		std::cout << e.what() << std::endl;
	}

	std::cout << std::endl << "creating a span and trying to add different numbers with addrange:" << std::endl;
	Span ra = Span(100);

	std::cout << std::endl << "trying to add too big container:" << std::endl;
	std::vector<int> vec0(101);

	try {
		ra.addNumber(vec0.begin(), vec0.end());
	} catch ( std::exception & e ) {
		std::cout << e.what() << std::endl;
	}

	std::cout << std::endl << "filling with random numbers:" << std::endl;
	std::vector<int> vec(100);
	srand(time(0));

	for (std::vector<int>::iterator ite = vec.begin(); ite < vec.end(); ++ite)
		*ite = rand() % 100000;

	try {
		ra.addNumber(vec.begin(), vec.end());
	} catch ( std::exception & e ) {
		std::cout << e.what() << std::endl;
	}
	//for (unsigned int i = 0; i < ra.getSize(); i++)
	//	std::cout << ra.getElem(i) << std::endl;

	std::cout << "longest span: " << ra.longestSpan() << std::endl;
	std::cout << "shortest span: " << ra.shortestSpan() << std::endl;

	return (0);
}