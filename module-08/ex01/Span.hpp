//
// Created by Antoine LANGLOIS on 2/28/22.
//

#ifndef PISCINE_CPP_SPAN_HPP
#define PISCINE_CPP_SPAN_HPP

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <set>
#include <climits>
#include <ctime>

class Span {

private:
	unsigned int					_size;
	std::vector<int>				_elem;

public:
									Span( void );
									~Span( void );
									Span( Span const & s );
									Span( unsigned int const & N );

	Span							&operator=( Span const & s );

	void 							addNumber( int const & nb );
	void 							addNumber( std::vector<int>::iterator a, std::vector<int>::iterator b );

	unsigned int					shortestSpan( void ) const;
	unsigned int					longestSpan( void ) const;

	unsigned int 					getSize() const;
	unsigned int 					getElem( unsigned int const & nb ) const;

	class EmptySpanException : public std::exception {
		public:
			virtual const char * what() const throw() {
				return ("Span too small");
			}
	};

	class FullSpanException : public std::exception {
		public:
			virtual const char * what() const throw() {
				return ("Span full");
		}
	};

	class OutOfRangeException : public std::exception {
	public:
		virtual const char * what() const throw() {
			return ("Out of range");
		}
	};


};


#endif //PISCINE_CPP_SPAN_HPP
