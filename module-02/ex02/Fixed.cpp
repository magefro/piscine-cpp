//
// Created by alangloi on 1/25/22.
//

#include "Fixed.hpp"

int const Fixed::_nbBits = 8;

//---------------------------- CONSTRUCTORS -----------------------------------

Fixed::Fixed() : _val(0) {

	std::cout << "Default constructor called" << std::endl;
	return ;
}

Fixed::~Fixed() {

	std::cout << "Destructor called" << std::endl;
	return ;
}

Fixed::Fixed( Fixed const & f ) {

	std::cout << "Copy constructor called" << std::endl;
	*this = f;
	return ;
}

Fixed::Fixed( int const nb ) {

	std::cout << "Integer Number constructor called" << std::endl;
	this->_val = nb * (1 << Fixed::_nbBits);
	return ;
}

Fixed::Fixed( float const nb ) {

	std::cout << "Floating Number constructor called" << std::endl;
	this->_val = roundf(nb * (1 << Fixed::_nbBits));
	return ;
}

//---------------------------- GETTER AND SETTER -----------------------------------

int Fixed::getRawBits( void ) const {

	return (this->_val);
}

void Fixed::setRawBits( int const raw ) {

	this->_val = raw;
}

//---------------------------- CONVERTERS -----------------------------------

float Fixed::toFloat( void ) const {

	float result;

	result = (float)(this->_val) / (1 << Fixed::_nbBits);
	return (result);
}

int Fixed::toInt( void ) const {

	int result;

	result = this->_val / (1 << Fixed::_nbBits);
	return (result);
}

//---------------------------- FUNCTIONS -----------------------------------

Fixed &Fixed::min( Fixed & f1, Fixed & f2 ) {

	if (f1 < f2)
		return (f1);
	return (f2);
};

const Fixed &Fixed::min( Fixed const & f1, Fixed const & f2 ) {

	if (f1 < f2)
		return (f1);
	return (f2);
};

Fixed &Fixed::max( Fixed & f1, Fixed & f2 ) {

	if (f1 > f2)
		return (f1);
	return (f2);
};

const Fixed &Fixed::max( Fixed const & f1, Fixed const & f2 ) {

	if (f1 > f2)
		return (f1);
	return (f2);
};

//---------------------------- OPERATORS -----------------------------------

//--------- COMPARISON --------
bool Fixed::operator>( Fixed const & f ) const {

	return (this->_val > f.getRawBits());
}

bool Fixed::operator<( Fixed const & f) const {

	return (this->_val < f.getRawBits());
}

bool Fixed::operator>=( Fixed const & f) const {

	return (this->_val >= f.getRawBits());
}

bool Fixed::operator<=( Fixed const & f ) const {

	return (this->_val <= f.getRawBits());
}

bool Fixed::operator==( Fixed const & f ) const {

	return (this->_val == f.getRawBits());
}

bool Fixed::operator!=( Fixed const & f ) const {

	return (this->_val != f.getRawBits());
}

//------ ARITHMETIC --------
Fixed Fixed::operator+( Fixed const & f ) const {

	Fixed tmp;

	tmp.setRawBits(this->_val + f.getRawBits());
	return (tmp);
}

Fixed Fixed::operator-( Fixed const & f ) const {

	Fixed tmp;

	tmp.setRawBits(this->_val - f.getRawBits());
	return (tmp);
}

Fixed Fixed::operator*( Fixed const & f ) const {

	return (Fixed(this->toFloat() * f.toFloat()));
}

Fixed Fixed::operator/( Fixed const & f ) const {

	return (Fixed(this->toFloat() / f.toFloat()));
}

//-------- INCREMENT / DECREMENT -------
Fixed &Fixed::operator++( void ) {

	++this->_val;
	return (*this);
}

Fixed &Fixed::operator--( void ) {

	--this->_val;
	return (*this);
}

Fixed Fixed::operator++( int ) {

	Fixed tmp;

	tmp._val = this->_val++;
	return (tmp);
}

Fixed Fixed::operator--( int ) {

	Fixed tmp;

	tmp._val = this->_val--;
	return (tmp);
}

//----------- ASSIGNATION --------
Fixed &Fixed::operator=( Fixed const & f ) {

	this->_val = f.getRawBits();
	return (*this);
}

std::ostream &operator<<( std::ostream & o, Fixed const & f ) {

	o << f.toFloat();
	return (o);
}