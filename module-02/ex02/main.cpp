//
// Created by alangloi on 1/25/22.
//

#include <iostream>
#include "Fixed.hpp"

int main( void ) {

	Fixed a;
	Fixed const b( Fixed( 5.05f ) * Fixed( 2 ) );

	std::cout << "INCREMENT / DECREMENT OPERATORS" << std::endl;
	std::cout << "--- pre increment a ---" << std::endl;
	std::cout << a << std::endl;
	std::cout << ++a << std::endl;
	std::cout << "--- post increment a ---" << std::endl;
	std::cout << a << std::endl;
	std::cout << a++ << std::endl;
	std::cout << "--- post decrement a ---" << std::endl;
	std::cout << a << std::endl;
	std::cout << a-- << std::endl;
	std::cout << "--- pre decrement a ---" << std::endl;
	std::cout << a << std::endl;
	std::cout << --a << std::endl;
	std::cout << "--- display b ---" << std::endl;
	std::cout << b << std::endl << std::endl << std::endl;
	a++;
	std::cout << "COMPARISON OPERATORS (a = " << a << " ,b = " << b << ")" << std::endl;
	std::cout << "--- a > b ---" << std::endl;
	std::cout << (a > b) << std::endl;
	std::cout << "--- a < b ---" << std::endl;
	std::cout << (a < b) << std::endl;
	std::cout << "--- a != b ---" << std::endl;
	std::cout << (a != b) << std::endl;
	std::cout << "--- a == b ---" << std::endl;
	std::cout << (a == b) << std::endl << std::endl;
	std::cout << "ARITHMETIC OPERATORS (a = " << a << " ,b = " << b << ")" << std::endl;
	std::cout << "--- a + b ---" << std::endl;
	std::cout << (a + b) << std::endl;
	std::cout << "--- a - b ---" << std::endl;
	std::cout << (a - b) << std::endl;
	std::cout << "--- a * b ---" << std::endl;
	std::cout << (a * b) << std::endl;
	std::cout << "--- a / b ---" << std::endl;
	std::cout << (a / b) << std::endl << std::endl;
	std::cout << "MIN / MAX FUNCTIONS (a = " << a << " ,b = " << b << ")" << std::endl;
	std::cout << "--- max ( a, b ) ---" << std::endl;
	std::cout << Fixed::max( a, b ) << std::endl;
	std::cout << "--- min ( a, b ) ---" << std::endl;
	std::cout << Fixed::min( a, b ) << std::endl;
	return 0;
}