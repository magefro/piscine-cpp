//
// Created by alangloi on 1/25/22.
//

#include "Fixed.hpp"
#include "Point.hpp"

float sign( Point const & p1, Point const & p2, Point const & p3 ) {

	return ( (p1.getX() - p3.getX()) *
			 (p2.getY() - p3.getY()) -
			 (p2.getX() - p3.getX()) *
			 (p1.getY() - p3.getY()) );
}

bool bsp( Point const & a, Point const & b, Point const & c, Point const & point) {

	float d1, d2, d3;
	bool isneg, ispos;

	d1 = sign(point, a, b);
	d2 = sign(point, b, c);
	d3 = sign(point, c, a);

	isneg = (d1 <= 0) || (d2 <= 0) || (d3 <= 0);
	ispos = (d1 >= 0) || (d2 >= 0) || (d3 >= 0);

	return (!(isneg && ispos));
}