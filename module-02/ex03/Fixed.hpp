//
// Created by alangloi on 1/25/22.
//

#ifndef PISCINE_CPP_FIXED_HPP
#define PISCINE_CPP_FIXED_HPP

#include <iostream>
#include <cmath>

class Fixed {

private:
	int _val;
	static int const _nbBits;

public:
	Fixed( void );
	~Fixed();
	Fixed( Fixed const & f );
	Fixed( int const nb );
	Fixed( float const nb );

	Fixed &operator=( Fixed const & f );

	bool operator>( Fixed const & f ) const;
	bool operator<( Fixed const & f) const;
	bool operator>=( Fixed const & f) const;
	bool operator<=( Fixed const & f ) const;
	bool operator==( Fixed const & f ) const;
	bool operator!=( Fixed const & f ) const;

	Fixed operator+( Fixed const & f ) const;
	Fixed operator-( Fixed const & f ) const;
	Fixed operator*( Fixed const & f ) const;
	Fixed operator/( Fixed const & f ) const;

	Fixed &operator++( void );
	Fixed &operator--( void );
	Fixed operator++( int );
	Fixed operator--( int );

	int getRawBits( void ) const;
	void setRawBits( int const raw );

	float toFloat( void ) const;
	int toInt( void ) const;

	static Fixed &min( Fixed & f1, Fixed & f2 );
	static Fixed &max( Fixed & f1, Fixed & f2 );

	static const Fixed &max( Fixed const & f1, Fixed const & f2 );
	static const Fixed &min( Fixed const & f1, Fixed const & f2 );
};

std::ostream & operator<<(std::ostream & o, Fixed const & f);

#endif //PISCINE_CPP_FIXED_HPP