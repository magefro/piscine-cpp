//
// Created by alangloi on 1/25/22.
//

#include "Point.hpp"
#include "Fixed.hpp"

int main( void ) {



	Point a( 10, 10);
	Point b(5, 20);
	Point c(15, 20);
	Point p(10,150000);

	std::cout << "a.x = " << a.getX() << std::endl;
	std::cout << "a.y = " << a.getY() << std::endl;
	std::cout << "b.x = " << b.getX() << std::endl;
	std::cout << "b.y = " << b.getY() << std::endl;
	std::cout << "c.x = " << c.getX() << std::endl;
	std::cout << "c.y = " << c.getY() << std::endl;

	std::cout << std::endl << "point is outside the triangle" << std::endl;

	std::cout << "p.x = " << p.getX() << std::endl;
	std::cout << "p.y = " << p.getY() << std::endl;

	if (bsp(a,b,c,p))
		std::cout << "Yes" << std::endl << std::endl;
	else
		std::cout << "No" << std::endl << std::endl;

	std::cout << std::endl << "point is inside the triangle" << std::endl;

	Point x(10,15);

	std::cout << "x.x = " << x.getX() << std::endl;
	std::cout << "x.y = " << x.getY() << std::endl;

	if (bsp(a,b,c,x))
		std::cout << "Yes" << std::endl << std::endl;
	else
		std::cout << "No" << std::endl << std::endl;

	std::cout << std::endl << "point is on the vertex" << std::endl;

	Point u(10,10);

	std::cout << "u.x = " << x.getX() << std::endl;
	std::cout << "u.y = " << x.getY() << std::endl;

	if (bsp(a,b,c,u))
		std::cout << "Yes" << std::endl << std::endl;
	else
		std::cout << "No" << std::endl << std::endl;

}