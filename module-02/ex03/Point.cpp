//
// Created by alangloi on 1/25/22.
//

#include "Point.hpp"

//---------------------------- CONSTRUCTORS ---------------------------------------------

Point::Point() : _x( 0 ), _y( 0 ) {
	return ;
}

Point::~Point() {
	return ;
}

Point::Point( Point const & p ) {

	//*this = p;
	(void)p;
	return ;
}

Point::Point( float const & x, float const & y) : _x( x ), _y( y ) {

	return ;
}

Point &Point::operator=(Point const & p ) {

	//this->_x = p._x;
	//this->_y = p._y;
	(void)p;
	return (*this);
}

//--------------------------- GETTERS AND SETTERS ------------------------------------

float Point::getX() const {

	return (this->_x.Fixed::toFloat());
}

float Point::getY() const {

	return (this->_y.Fixed::toFloat());
}
