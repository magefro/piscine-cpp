//
// Created by alangloi on 1/25/22.
//

#ifndef PISCINE_CPP_POINT_HPP
#define PISCINE_CPP_POINT_HPP

#include	"Fixed.hpp"

class Point {

private:
	Fixed const _x;
	Fixed const _y;

public:
	Point();
	~Point();
	Point( Point const & p );
	Point( float const & x, float const & y);

	Point &operator=(Point const & p );

	float getX() const;
	float getY() const;

};

bool	bsp( Point const & a, Point const & b, Point const & c, Point const & point);
float	sign( Point const & p1, Point const & p2, Point const & p3 );

#endif //PISCINE_CPP_POINT_HPP
