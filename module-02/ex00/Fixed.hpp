//
// Created by alangloi on 1/25/22.
//

#ifndef PISCINE_CPP_FIXED_HPP
#define PISCINE_CPP_FIXED_HPP

#include <iostream>

class Fixed {

private:
	int _val;
	static int _nbBits;

public:
	Fixed( void );
	~Fixed();
	Fixed( Fixed const & f );
	Fixed &operator=( Fixed const & f );

	int getRawBits( void ) const;
	void setRawBits( int const raw );

};

#endif //PISCINE_CPP_FIXED_HPP
