//
// Created by alangloi on 1/25/22.
//

#include "Fixed.hpp"

int _nbBits = 8;

//------------------------ CONSTRUCTORS ------------------------

Fixed::Fixed() : _val(0) {

	std::cout << "Default constructor called" << std::endl;
	return ;
}

Fixed::~Fixed() {

	std::cout << "Destructor called" << std::endl;
	return ;
}

Fixed::Fixed( Fixed const & f ) {

	std::cout << "Copy constructor called" << std::endl;
	*this = f;
	return ;
}

// --------------------- OPERATORS ---------------------------

Fixed &Fixed::operator=( Fixed const & f ) {

	std::cout << "Assignation operator called" << std::endl;
	this->_val = f.getRawBits();
	return (*this);
}


//------------------- GETTERS AND SETTERS ----------------------

int Fixed::getRawBits( void ) const {

	std::cout << "getRawBits member function called" << std::endl;
	return (this->_val);
}

void Fixed::setRawBits( int const raw ) {

	std::cout << "setRawBits member function called" << std::endl;
	this->_val = raw;
	return ;
}