//
// Created by alangloi on 1/25/22.
//

#include "Fixed.hpp"

int const Fixed::_nbBits = 8;

Fixed::Fixed() : _val(0) {

	std::cout << "Default constructor called" << std::endl;
	return ;
}

Fixed::~Fixed() {

	std::cout << "Destructor called" << std::endl;
	return ;
}

Fixed::Fixed( Fixed const & f ) {

	std::cout << "Copy constructor called" << std::endl;
	*this = f;
	return ;
}

Fixed::Fixed( int const nb ) {

	std::cout << "Int constructor called" << std::endl;
	this->_val = nb * (1 << this->_nbBits);
}

Fixed::Fixed( float const nb ) {

	std::cout << "Float constructor called" << std::endl;
	this->_val = roundf(nb * (1 << this->_nbBits));
}

Fixed &Fixed::operator=( Fixed const & f ) {

	std::cout << "Copy assignment operator called" << std::endl;
	this->_val = f.getRawBits();
	return (*this);
}

std::ostream &operator<<( std::ostream & o, Fixed const & f ) {

	o << f.toFloat();
	return (o);
}

int Fixed::getRawBits( void ) const {

	return (this->_val);
}

void Fixed::setRawBits( int const raw ) {

	this->_val = raw;
}

float Fixed::toFloat( void ) const {

	float result;

	result = (float)(this->_val) / (1 << this->_nbBits);
	return (result);
}

int Fixed::toInt( void ) const {

	int result;

	result = this->_val / (1 << this->_nbBits);
	return (result);
}
