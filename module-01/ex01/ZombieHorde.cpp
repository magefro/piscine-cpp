#include "Zombie.hpp"


Zombie* newZombie( int id, std::string name ) {

	Zombie *zombie = new Zombie( id, name );
	return (zombie);
}


Zombie* zombieHorde( int N, std::string name ) {

	int i = 0;

	Zombie *zombies = new Zombie[N];
	while (i < N)
	{
		zombies[i].setZombieName( name, i );
		i++;
	}
	return (zombies);
}
