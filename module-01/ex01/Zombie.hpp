#include <iostream>
#include <string>
#include <sstream>

#ifndef PISCINE_CPP_ZOMBIE_H
#define PISCINE_CPP_ZOMBIE_H

class Zombie {

private:
	std::string _name;

public:
	Zombie( int id, std::string name );
	Zombie( void );
	~Zombie( void );

	void announce();
	void	setZombieName( std::string name, int id );
};

Zombie *zombieHorde ( int N, std::string name );

#endif //PISCINE_CPP_ZOMBIE_H
