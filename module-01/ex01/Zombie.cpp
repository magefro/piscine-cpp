#include "Zombie.hpp"

Zombie::Zombie( void ) {

	std::cout << "Zombie created" << std::endl;
	return ;
}

Zombie::~Zombie( void ) {

	std::cout << this->_name << " exploded" << std::endl;
	return ;
}

Zombie::Zombie( int id, std::string name ) {

	std::string s;
	std::stringstream out;
	out << id;
	s = out.str();
	name.append(s);
	this->_name = name;
	std::cout << this->_name << " created :)" << std::endl;
	return ;
}

void Zombie::announce( void ) {

	std::cout << this->_name << ": BraiiiiiiinnnzzzZ..." << std::endl;
}

void Zombie::setZombieName( std::string name, int id ) {

	std::string s;
	std::stringstream out;
	out << id;
	s = out.str();
	name.append(s);
	this->_name = name;
	std::cout << "Zombie number " << id << " got a new name: " << this->_name << std::endl;
	return ;
}