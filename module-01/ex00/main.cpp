#include "Zombie.hpp"

int main() {
	Zombie *zombie1 = newZombie("Carpenter");
	Zombie *zombie2 = newZombie("Rob");
	Zombie *zombie3 = newZombie("Shaun");

	randomChump("Abricot");
	randomChump("Champagne");
	randomChump("Abracadabra");
	randomChump("Baleze");

	delete zombie1;
	delete zombie2;
	delete zombie3;
	return (0);
}