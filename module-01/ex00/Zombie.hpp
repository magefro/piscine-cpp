#include <iostream>

#ifndef PISCINE_CPP_ZOMBIE_CLASS_HPP
#define PISCINE_CPP_ZOMBIE_CLASS_HPP

class Zombie {

private:
	std::string	_name;

public:
	Zombie( std::string name );
	~Zombie( void );

	void	announce( void );
};

Zombie	*newZombie( std::string name );
void	randomChump( std:: string name );

#endif //PISCINE_CPP_ZOMBIE_CLASS_HPP
