#include "Zombie.hpp"

Zombie::~Zombie ( void ) {
	std::cout << this->_name << " exploded." << std::endl;
	return ;
}

void	Zombie::announce ( void ) {
	std::cout << this->_name << ": BraiiiiiiinnnzzzZ..." << std::endl;
}

Zombie::Zombie( std::string name ) {
	this->_name = name;
	return ;
}
