//
// Created by alangloi on 1/25/22.
//

#include "Karen.hpp"

Karen::Karen()  {
	levels[0] = "DEBUG";
	levels[1] = "INFO";
	levels[2] = "WARNING";
	levels[3] = "ERROR";
	return ;
}

Karen::~Karen() {
	return ;
}

void Karen::_debug( void ) {
	std::cout << "print debug" << std::endl;
}

void Karen::_info( void ) {
	std::cout << "print info" << std::endl;
}

void Karen::_warning( void ) {
	std::cout << "print warning" << std::endl;
}

void Karen::_error( void ) {
	std::cout << "print error" << std::endl;
}

void Karen::complain( std::string level ) {
	void (Karen::*funcPtr[4])( void ) = {
			&Karen::_debug,
			&Karen::_info,
			&Karen::_warning,
			&Karen::_error
	};
	/*
	std::string levels[4] = {
			"DEBUG",
			"INFO",
			"WARNING",
			"ERROR"
	};
	*/
	int index = 0;

	while (index < 4)
	{
		if (level == levels[index])
		{
			(this->*funcPtr[index])();
			return ;
		}
		index++;
	}
}
