//
// Created by alangloi on 1/25/22.
//

#include "Karen.hpp"

int main( int argc, char **argv )
{
	Karen karen;
	int index;

	if (argc != 2)
	{
		std::cout << "Wrong number of arguments." << std::endl;
		return (1);
	}
	else
	{
		for (index = 0; index < 4; index++)
		{
			if (argv[1] == karen.levels[index]) {
				break ;
			}
		}
		switch (index) {
			case 0 :
				karen.complain("DEBUG");
			case 1 :
				karen.complain("INFO");
			case 2 :
				karen.complain("WARNING");
			case 3 :
				karen.complain("ERROR");
				break ;
			default:
				std::cout << "[ ... ]" << std::endl;
		}
		return (0);
	}




}
