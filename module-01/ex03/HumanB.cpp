#include "HumanB.hpp"

HumanB::~HumanB( void ) {
	return ;
}

void HumanB::attack( void ) {

	if (this->_weapon == NULL)
	{
		std::cout << this->_name << " attacks with their hands" << std::endl;
		return ;
	}
	std::cout 	<< this->_name << " attacks with their "
				<< this->_weapon->getType() << std::endl;
}

HumanB::HumanB( std::string name ) : _name(name) {

	this->_weapon = NULL;
	return ;
}

void HumanB::setWeapon( Weapon& weapon ) {

	this->_weapon = &weapon;
}