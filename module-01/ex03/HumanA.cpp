#include "HumanA.hpp"

HumanA::~HumanA( void ) {

	return ;
}

HumanA::HumanA( std::string name, Weapon &weapon ) : _name(name), _weapon(weapon) {

	return ;
}

void HumanA::attack( void ) {

	std::cout 	<< this->_name << " attacks with their "
				<< this->_weapon.getType() << std::endl;
}

void HumanA::setWeapon( Weapon& weapon ) {

	this->_weapon = weapon;
}