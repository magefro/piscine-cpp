#include <iostream>
#include <string>

#ifndef PISCINE_CPP_WEAPON_HPP
#define PISCINE_CPP_WEAPON_HPP

class Weapon {

private:
	std::string _type;

public:
	Weapon( void );
	~Weapon( void );
	Weapon( std::string type );

	std::string const &getType( void ) const ;
	void setType( std::string type );
};

#endif //PISCINE_CPP_WEAPON_HPP
