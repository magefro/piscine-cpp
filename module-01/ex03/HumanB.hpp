#ifndef PISCINE_CPP_HUMANB_HPP
#define PISCINE_CPP_HUMANB_HPP

#include "Weapon.hpp"

class HumanB {

private:
	std::string _name;
	Weapon		*_weapon;

public:
	HumanB( std::string name );
	~HumanB( void );

	void attack( void );
	void setWeapon( Weapon &weapon );
};


#endif //PISCINE_CPP_HUMANB_HPP
