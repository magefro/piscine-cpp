//
// Created by alangloi on 1/25/22.
//

#include <iostream>
#include <string>

#ifndef PISCINE_CPP_KAREN_HPP
#define PISCINE_CPP_KAREN_HPP

class Karen {
private:
	void _debug( void );
	void _info( void );
	void _warning( void );
	void _error( void );

public:
	Karen();
	~Karen();

	void complain( std::string level );
};

#endif //PISCINE_CPP_KAREN_HPP
