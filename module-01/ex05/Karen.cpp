//
// Created by alangloi on 1/25/22.
//

#include "Karen.hpp"

Karen::Karen() {
	return ;
}

Karen::~Karen() {
	return ;
}

void Karen::_debug( void ) {

	std::cout << "[ DEBUG ]" << std::endl;
	std::cout << "print debug" << std::endl;
	return ;
}

void Karen::_info( void ) {

	std::cout << "[ INFO ]" << std::endl;
	std::cout << "print info" << std::endl;
	return ;
}

void Karen::_warning( void ) {

	std::cout << "[ WARNING ]" << std::endl;
	std::cout << "print warning" << std::endl;
	return ;
}

void Karen::_error( void ) {

	std::cout << "[ ERROR ]" << std::endl;
	std::cout << "print error" << std::endl;
	return ;
}

void Karen::complain( std::string level ) {
	void (Karen::*funcPtr[4])( void ) = {
			&Karen::_debug,
			&Karen::_info,
			&Karen::_warning,
			&Karen::_error
	};
	std::string levels[4] = { "DEBUG", "INFO", "WARNING", "ERROR"};
	int index = 0;

	while (index < 4)
	{
		if (level == levels[index])
		{
			(this->*funcPtr[index])();
			return ;
		}
		index++;
	}
}