#include <iostream>
#include <string>

int main() {
	std::string stringSTR = "HI THIS IS BRAIN";
	std::string *stringPTR = &stringSTR;
	std::string &stringREF = stringSTR;

	std::cout << "memory address of the string variable: " << &stringSTR << std::endl;
	std::cout << "memory address held by stringPTR: " << stringPTR << std::endl;
	std::cout << "memory address held by stringREF: " << &stringREF << std::endl;
	std::cout << "value of the string variable: " << stringSTR << std::endl;
	std::cout << "value pointed to by stringPTR: " << *stringPTR << std::endl;
	std::cout << "value pointed to by stringREF: " << stringREF << std::endl;
	return (0);
}