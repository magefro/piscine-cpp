//
// Created by alangloi on 1/24/22.
//

#include <iostream>
#include <fstream>
#include <string>

#ifndef PISCINE_CPP_SEDISFORLOSERS_HPP
#define PISCINE_CPP_SEDISFORLOSERS_HPP

class SedIsForLosers {

private:
	std::string _s1;
	std::string _s2;

public:
	SedIsForLosers( std::string const& s1, std::string const& s2 );
	SedIsForLosers( void );
	~SedIsForLosers( void );

	void ReplaceText( std::ifstream& input_file, std::ofstream& output_file );
};

#endif //PISCINE_CPP_SEDISFORLOSERS_HPP
