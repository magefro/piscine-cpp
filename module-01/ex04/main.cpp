//
// Created by alangloi on 1/24/22.
//

#include "SedIsForLosers.hpp"

int main( int argc, char **argv )
{
	if (argc != 4)
	{
		std::cout << "Wrong number of arguments (Filename, string 1, string 2)." << std::endl;
		return (1);
	}
	else
	{
		std::ifstream input_file(argv[1]);

		//check if file exists
		if (!input_file.good())
		{
			std::cout << "Can't open file" << std::endl;
			return (1);
		}

		std::string s1(argv[2]);
		std::string s2(argv[3]);

		//check if string 1 is not null
		if (s1.length() == 0)
		{
			std::cout << "replace string can't be empty" << std::endl;
			return (1);
		}

		//rename new file
		std::string output_name(argv[1]);
		output_name.append(".replace");
		std::ofstream output_file(output_name.c_str());

		SedIsForLosers result(s1, s2);

		//execute function that will replace text
		result.SedIsForLosers::ReplaceText( input_file, output_file );
		output_file.close();
		input_file.close();
	}
	return (0);
}