//
// Created by alangloi on 1/24/22.
//

#include "SedIsForLosers.hpp"

SedIsForLosers::SedIsForLosers( std::string const& s1, std::string const& s2 ) : _s1(s1), _s2(s2) {
	return ;
}

SedIsForLosers::SedIsForLosers( void ) {
	return ;
}

SedIsForLosers::~SedIsForLosers( void ) {
	return ;
}

void SedIsForLosers::ReplaceText( std::ifstream& input_file, std::ofstream& output_file ) {
	std::string output_text;
	std::string new_output_text;

	while (std::getline(input_file, output_text))
	{
		std::string tmp;
		size_t len;
		size_t pos = 0;
		if (output_text.find(this->_s1) != std::string::npos)
		{
			while (output_text[pos])
			{
				tmp = output_text.substr(pos);
				if ((len = tmp.find(this->_s1)) != std::string::npos)
				{
					new_output_text += output_text.substr(pos, len);
					new_output_text += this->_s2;
					pos += len;
					pos += this->_s1.length();
				}
				else
				{
					new_output_text += output_text.substr(pos);
					break ;
				}
			}
		}
		else
			new_output_text = output_text;
		output_file << new_output_text << std::endl;
	}
}
